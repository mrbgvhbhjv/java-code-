package File_System_operate;

import java.io.File;

public class demo6 {
    public static void main(String[] args) {
        File file_Directory = new File("C:/User/111");
        File file_File = new File("C:/User/111/test.txt");

        // 判断 C:/User/111 路径下的 111 是 目录 或 文件
        System.out.println(file_Directory.isDirectory());
        System.out.println(file_Directory.isFile());

        // 判断 C:/User/111/test.txt 路径下的 test.txt 是 目录 或 文件
        System.out.println(file_File.isDirectory());
        System.out.println(file_File.isFile());
    }
}
