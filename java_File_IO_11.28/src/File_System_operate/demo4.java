package File_System_operate;

import java.io.File;
import java.io.IOException;

public class demo4 {
    public static void main(String[] args) throws IOException {
        File file = new File("./text.txt");
        System.out.println(file.getParent()); // 返回文件所在的父目录的路径
        System.out.println(file.getName());   // 返回文件的纯文本名称
        System.out.println(file.getPath());  // 返回 File 对象的⽂件路径
        System.out.println(file.getAbsolutePath()); //返回 File 对象的绝对路径
        System.out.println(file.getCanonicalPath()); //返回 File 对象的修饰过的绝对路径
    }
}
