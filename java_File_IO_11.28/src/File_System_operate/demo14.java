package File_System_operate;

import java.io.File;
import java.io.IOException;

public class demo14 {
    public static void main(String[] args) throws IOException {
        // ./ 表示的是当前项目路径：E:\code\gitee\JAVA代码\java-code-\java_File_IO_11.28
        File file = new File("./test.txt");
        System.out.println("创建文件：");
        System.out.println("文件是否创建成功："+file.createNewFile());
        File new_file = new File("./test2.txt");

        System.out.println("=====================");
        System.out.println("文件重命名：");
        System.out.println("文件是否重命名成功："+ file.renameTo(new_file));
    }
}
