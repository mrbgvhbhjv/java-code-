package File_content_operate;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class demo2 {
    public static void main(String[] args) throws IOException {
        try(InputStream inputStream = new FileInputStream("./test.txt")) {
            while (true) {
                //读取文件操作
                byte[] bytes = new byte[3];
//            这个代码里面，有两个返回值，一个是 n，一个是 bytes数组（输出型参数）
                int n = inputStream.read(bytes);
//            判断文件是否已经读取完毕
                if (n == -1) {

                    break;
                }
                for (int i = 0; i <n; i++) {
                    System.out.printf("%X\n",bytes[i]);
                }
                System.out.println("======================");
            }
        }
    }
}
