package File_content_operate;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;


public class demo8 {
    public static void main(String[] args) {
        try(Writer writer = new FileWriter("./output.txt")) {
            writer.write(97);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
