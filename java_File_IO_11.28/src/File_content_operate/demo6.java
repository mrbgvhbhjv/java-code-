package File_content_operate;


import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class demo6 {
    public static void main(String[] args) {
        try(OutputStream outputStream = new FileOutputStream("./output.txt",true)) {
            //操作文件
            //创建字节数组，往里面存放数据 a， b， c， d， e
            byte[] bytes = new byte[]{97,98,99,100,101};

            //从 1 下标开始进行写写操作，只写 3 个字节大小的数据
            outputStream.write(bytes,1,3);

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
