package File_content_operate;


import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

public class demo9 {
    public static void main(String[] args) {
        try(Reader reader = new FileReader("./test.txt")) {
            char[] buf = new char[1024];
            while(true) {
                int n = reader.read(buf);
                if (n == -1) {
                    break;
                }
//                注意这里的循环的条件，是 i<n,n为读取到的字符的个数
                for (int i = 0;i < n;i++) {
                    System.out.println(buf[i]);
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
