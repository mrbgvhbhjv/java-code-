package File_content_operate;


import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class demo5 {
    public static void main(String[] args) {
        try(OutputStream outputStream = new FileOutputStream("./output.txt")) {
//      操作文件

            byte[] bytes = new byte[]{99,98,97};
            outputStream.write(bytes);

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
