package File_content_operate;


import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

public class demo10 {
    public static void main(String[] args) {
        try(Reader reader = new FileReader("./test.txt")) {
            char[] buf = new char[1024];
            while(true) {
                int leng = reader.read(buf,1,2);
                System.out.println("leng = " + leng);
                if (leng == -1) {
                    break;
                }
                for (int i = 1; i <= leng; i++) {
                    System.out.println(buf[i]);
                }
                System.out.println("================");
            }
            System.out.println(buf[1]);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
