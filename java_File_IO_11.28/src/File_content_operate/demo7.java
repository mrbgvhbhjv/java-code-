package File_content_operate;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

public class demo7 {
    public static void main(String[] args) {
        try(Reader reader = new FileReader("./test.txt")){

            while (true) {
                int c = reader.read();
                if (c == -1) {
                    break;
                }
                System.out.println((char) c);
//                注意：这里需要进行强制类型转换
            }

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
