package File_content_operate;


import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class demo1 {
    public static void main(String[] args) throws IOException {

        try(InputStream inputStream = new FileInputStream("./test.txt")) {
            // 读取文件操作：
            while (true) {
                int data = inputStream.read();
                // 返回 -1 ，说明文件内容已经读取完毕
                if (data == -1) {
                    break;
                }
                System.out.printf("%X\n",data);
            }
        }
    }
}
