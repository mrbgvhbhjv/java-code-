package File_content_operate;


import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class demo3 {
    public static void main(String[] args) throws IOException {
//使用 try with resource 的语法，打开文件，并程序结束的时候，自动执行 close()方法
        try(InputStream inputStream = new FileInputStream("./test.txt")) {
            while (true) {
//              操作文件
//              定义空白字节数组
                byte[] bytes = new byte[1024];

//                读取文件
                int n = inputStream.read(bytes,0,3);
//                查看你本次读取到了多少个字节
                System.out.println("n = " + n);
//                判断是否已经读取结束
                if (n == -1) {
                    break;
                }
//                输出读取后，存放到字节数组中的内容
                for (int i = 0; i < n; i++) {
                    System.out.printf("%X\n",bytes[i]);
                }
                System.out.println("===================");
            }

        }
    }
}
