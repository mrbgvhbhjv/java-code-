package File_content_operate;


import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class demo4 {
    public static void main(String[] args) {
        try(OutputStream outputStream = new FileOutputStream("./output.txt")){
//            操作文件
//            向output.txt文件当中，写入 a，b，c 三个字母
            outputStream.write(97);
            outputStream.write(98);
            outputStream.write(99);


        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
