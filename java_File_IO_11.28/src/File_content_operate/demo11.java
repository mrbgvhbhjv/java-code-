package File_content_operate;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class demo11 {
    public static void main(String[] args) {
        try(Writer writer = new FileWriter("./output.txt")) {
            char[] chars = new char[]{'a','b','c'};
            writer.write(chars);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
