package File_content_operate;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class demo12 {
    public static void main(String[] args) {
        try(Writer writer = new FileWriter("./output.txt")) {
//            先创建字符串对象，然后再写入
//            String s = new String("Hello World!");
//            writer.write(s);

//            直接在 write方法括号里 指定字符串
            writer.write("Hello World!!!");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
