package File_comprehensive_example;

import java.io.File;
import java.util.Scanner;

public class demo1 {
    public static void main(String[] args) {
//        准备工作：
//        第一步：先让用户输入一个指定的目录，搜索该目录里面的文件
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入你要搜索的目录路径(绝对 or 相对)：");
        String rootDir = scanner.next();

//        第二步：根据用户输入的目录路径，创建 File对象
        File rootfile = new File(rootDir);
//        其次，判断用户输入的目录路径，是否是一个目录
        if(!rootfile.isDirectory()) {
            System.out.println("输入的不是目录！");
            return;
        }

//        第三步：让用户输入要删除的文件中，文件名包含的关键字
        System.out.println("请输入要删除文件中包含的关键字：");
        String keyword = scanner.next();

//        开始遍历目录
        scanDir(rootfile,keyword);
    }

    private static void scanDir(File rootDir, String keyword) {
        System.out.println("搜索目录：" + rootDir.getAbsolutePath());
//        第一步：获取到目录中的所有文件（File对象）
        File[] files = rootDir.listFiles();
//        判断当前目录是否为空目录
        if (files == null) {
//            当前目录为空
            return;
        }

//        第二步：遍历当前目录中的内容
        for (File file:files) {
//            System.out.println("搜索目录 & 文件：" + file.getAbsolutePath());
//            判断当前文件，是普通文件，还是目录
            if (file.isFile()) {
//            1.如果是普通文件，则调用deleteFile方法，进行判定并选择删除操作
                deleteFile(file,keyword);
            }else {
//            2.如果是目录，则递归调用本方法
                scanDir(file,keyword);
            }
        }
    }

    static void deleteFile(File file, String keyword) {
//        进行判定，是否包含要删除文件的关键字
        if (file.getName().contains(keyword)) {
//            该文件的文件名包含关键字
            System.out.println("发现文件：" + file.getAbsolutePath() + "， 包含关键字");
            System.out.println("是否要删除该文件？（ y / n）：");

            Scanner scanner = new Scanner(System.in);
            String input = scanner.next();
//            根据输入的字符，选择是否删除文件
            if (input.equalsIgnoreCase("y")) {
                file.delete();
                System.out.println("文件已删除！");
            }else {
                System.out.println("您选择不删除文件！");
                return;
            }
        }
    }
}
