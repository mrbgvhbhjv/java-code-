package File_comprehensive_example;

import java.io.*;
import java.util.Scanner;


public class demo4 {
    public static void main(String[] args) {
//        准备工作：
//        第一步：输入指定的目录路径
        Scanner scanner = new Scanner(System.in);
        System.out.println("输入你要搜索的目录的路径（绝对 or 相对）：");
        String rootDir = scanner.next();

//        第二步：根据输入的目录路径，创建File文件
        File rootfile = new File(rootDir);
//        其次，判断这个文件是否为一个目录,不是目录，则退出程序
        if (!rootfile.isDirectory()){
            System.out.println("你输入的路径不是一个目录！");
            return;
        }

//        第三步：输入关键字
        System.out.println("请输入目录中的文件的文件名所包含的关键字：");
        String keyword = scanner.next();

        scanDir(rootfile,keyword);
    }

    private static void scanDir(File rootfile, String keyword) {
//        第一步：使用listFiles()获取到目录中的各个文件
        File[] files = rootfile.listFiles();
//        判断目录是否为空目录
        if (files == null) {
            return;
        }

//        第二步：对每个文件进行判定,判断是不是空目录
        for (File file:files) {
            if (file.isFile()) {
//                如果这个文件是一个普通文件，进行文件名和文件内容的判定
                dealFile(file, keyword);
            } else {
//                如果不是文件，就应该是目录，使用递归的方式，深度搜索
                scanDir(file,keyword);
            }
        }
    }

    private static void dealFile(File file, String keyword) {
//        1.判定文件名是否包含关键字
        if (file.getName().contains(keyword)) {
            System.out.println("文件名包含关键字：" + file.getAbsolutePath());
            return;
        }
//        2.判定文件内容是否包含关键字
//        设置 StringBuilder,把读到的字符内容拼接到StringBuilder
            StringBuilder stringBuilder = new StringBuilder();
//        读取文件内容的操作
//        由于keyword是字符串，就按照字符流的方式来进行处理
        try(Reader reader = new FileReader(file)) {
            while (true) {
                char[] chars = new char[1024];
                int n = reader.read(chars);
//                判断是否读取完毕
                if (n == -1) {
                    break;
                }
//                把读到的每一段内容，都进行一个追加操作
//                读到多少，追加多少
                stringBuilder.append(chars,0,n);
            }

//            判断 StringBuilder 是否包含关键字

////            第一种判断方式：
//            if (stringBuilder.toString().contains(keyword)) {
////                包含关键字
//                System.out.println("文件内容包含关键字：" + file.getAbsolutePath());
//            }

//            第二种判断方式：
            if (stringBuilder.indexOf(keyword) >= 0) {
//                包含关键字
                System.out.println("文件内容包含关键字：" + file.getAbsolutePath());
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return;
    }
}
