package File_comprehensive_example;

import java.io.*;
import java.util.Scanner;


public class demo3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
//        准备工作：
//        第一步：输入两个文件路径
        System.out.println("请输入源文件路径：");
        String srcPath = scanner.next();
//        目标文件，指的是复制出来后的文件存放的位置和文件名
        System.out.println("请输入目标文件路径：");
        String destPath = scanner.next();

//        第二步：根据输入的路径，创建File对象，并对这两个对象，进行验证判断
        File srcFile = new File(srcPath);
        if (!(srcFile.exists() && srcFile.isFile())) {
            System.out.println("源文件不存在 或 不是普通文件！");
            return;
        }

        File destFile = new File(destPath);
//        destFile可以不存在，在写文件的时候，会自动创建的
//        但是，destFile所在的目录，必须要存在，否则无法自动创建文件
        if (!destFile.getParentFile().exists()) {
            System.out.println("目标文件所在的目录不存在！");
            return;
        }

//        第三步：真正的进行文件的复制
//        此处我们不应该使用追加写，需要确保目标文件和源文件的大小一摸一样
        try(InputStream inputStream = new FileInputStream(srcFile);
            OutputStream outputStream = new FileOutputStream(destFile)){

            while (true) {
                byte[] bytes = new byte[1024];
                int n = inputStream.read(bytes);
//                对 n 进行判断，是否为 -1，也就是是否读完了源文件的内容
                if (n == -1) {
                    break;
                }

//                此处的write，不应该写整一个bytes数组
//                bytes数组不一定被填满，要按照实际的 n 这个长度来写入
//                都多少，写多少
                outputStream.write(bytes,0,n);
            }

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
