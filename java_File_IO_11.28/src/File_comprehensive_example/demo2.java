package File_comprehensive_example;

import java.io.File;
import java.util.Scanner;

public class demo2 {
    public static void main(String[] args) {
//        准备工作：
//        第一步：输入指定的目录路径
        Scanner scanner = new Scanner(System.in);
        System.out.println("输入你要搜索的目录的路径（绝对 or 相对）：");
        String rootDir = scanner.next();

//        第二步：根据输入的目录路径，创建File文件
        File rootfile = new File(rootDir);
//        其次，判断这个文件是否为一个目录,不是目录，则退出程序
        if (!rootfile.isDirectory()){
            System.out.println("你输入的路径所表示的不是一个目录！");
            return;
        }

//        第三步：输入关键字
        System.out.println("请输入目录中的文件的文件名所包含的关键字：");
        String keyword = scanner.next();

//        开始遍历
        scanFile(rootfile,keyword);

    }

    private static void scanFile(File rootfile, String keyword) {
//        第一步：使用listFiles()获取到目录中的各个文件
        File[] files = rootfile.listFiles();

//        第二步：对每个文件进行判定,判断是不是空目录
        for (File file:files) {
            if (file.isFile()){
//                如果这个文件是一个普通文件，就进行删除判定
                deleteFile(file, keyword);
            } else {
//                如果不是文件，就应该是目录，使用递归的方式，深度搜索
                scanFile(file,keyword);
            }
        }
    }

    private static void deleteFile(File file, String keyword) {
//        进入此方法，说明该文件是一个普通的文件了，接着判断文件名是否包含关键字
        if (file.getName().contains(keyword)) {
//            文件名包含关键字
            System.out.println("发现文件：" +file.getAbsolutePath() + "，是否要删除该文件（y/n）：");
            Scanner scanner = new Scanner(System.in);
            String s = scanner.next();

            if (s.equalsIgnoreCase("y")) {
                file.delete();
                System.out.println("你已删除该文件！");
            }else {
                System.out.println("你不删除该文件！");
            }
        }
    }
}
