package demo1;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-09-01
 * Time: 10:49
 */
public class Test {
    public static void drawMap(Shape shape) {
        shape.draw();
    }
    public static void main(String[] args) {
        //以下两个new的对象 叫做：匿名对象
        //匿名对象：只能使用一次的对象
        drawMap(new Cycle());
        drawMap(new Rect());
    }
}
