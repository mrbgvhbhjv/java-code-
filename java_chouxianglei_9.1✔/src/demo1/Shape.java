package demo1;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-09-01
 * Time: 10:49
 */
public abstract class Shape {//shape是个抽象类
    public abstract void draw();//draw是个抽象方法。
}
