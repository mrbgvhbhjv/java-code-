package demo3;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-09-01
 * Time: 21:13
 */
public class Dog extends Animal{
    public Dog(String name,int age) {
        super(name,age);
    }
    public void eat() {
        System.out.println(this.name+" 正在吃狗粮...");
    }
}
