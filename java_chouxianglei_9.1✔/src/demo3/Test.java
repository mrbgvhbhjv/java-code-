package demo3;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-09-01
 * Time: 21:11
 */
public class Test {
    public static void func(Animal animal) {
        animal.eat();
    }
    public static void main(String[] args) {
        Dog dog1 = new Dog("旺财",1);
        Cat cat1 = new Cat("小虎",2);
//        func(dog1);
//        func(cat1);

        Animal[] animals = {dog1,cat1,cat1,dog1};
//        for (Animal animal: animals) {
//            animal.eat();
//        }

        for (int i = 0; i < animals.length; i++) {
            Animal animal = animals[i];
            animal.eat();
        }
    }
}
