package demo2;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-09-01
 * Time: 11:11
 */
public class Noodle extends Food{
    @Override
    public void describe() {
        System.out.println("面条是很多人喜欢的食物之一。");
    }
}
