package demo2;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-09-01
 * Time: 11:10
 */
public abstract class Food {
    //抽象方法
    public abstract void describe();
}
