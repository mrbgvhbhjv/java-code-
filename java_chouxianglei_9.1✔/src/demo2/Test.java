package demo2;

import demo1.Rect;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-09-01
 * Time: 11:11
 */
public class Test {
    public static void describeFood(Food food) {
        food.describe();
    }
    public static void main(String[] args) {
        describeFood(new Rice());
        describeFood(new Noodle());

    }
}
