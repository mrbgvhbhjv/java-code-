/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-05-17
 * Time: 19:15
 */

//如何定义一个类？
//定义类的语法格式：通过关键字：class
// class 类名 ｛
//  成员变量/属性/字段
//
//   成员方法/行为
//    }

//    //成员变量
//    //成员变量 未初始化，可以使用，因为成员变量有 默认值。
//    //遵循的规则
////        1.引用类型：默认为null（空指针）
////        2.基本数据类型：默认为 0；

//class Dog {
//    public String name;
//    public int age;
//    //成员方法
//    public void eat() {
//        System.out.println(name+"吃狗粮");
//    }
//    public void shout() {
//        System.out.println(name+"狗叫");
//    }
//    public void DogSet(String n,int a) {
//        name = n;
//        age = a;
//    }
//
//}

    class Person{

        public String name;
        public int age;
        public int high;
        public String tel;

        public void Setperson(Person this,String n,int a,int h,String t) {
            name = n;
            age = a;
            high = h;
            tel = t;
        }
        public void eat() {
            System.out.println(name+"吃东西");
        }
        public void print() {
            System.out.println("年龄是："+age+"岁");
            System.out.println("身高是："+high+"厘米");
            System.out.println("手机号码是："+tel);
        }
}
public class test1 {
    //练习：定义一个狗类。
    public static void main1(String[] args) {
        //类的 实例化 ： 由 类 生成 对象 的过程。
//        Dog dog = new Dog();
//
//        //成员变量数值操作   访问方式： 对象的引用（引用变量）.成员变量（成员方法）
//        System.out.println(dog.name);//没赋值 默认值为 null
//        System.out.println(dog.age);//没赋值 默认值为 0
//        dog.name = "小黑";
//        dog.age = 1;
//        System.out.println(dog.name);
//        System.out.println(dog.age);
//
//        dog.eat();
//        dog.shout();
//
//        dog.DogSet("小黄",3);
//        dog.eat();
//        dog.shout();
    }

    public static void main(String[] args) {
        //练习：定义一个类，去描述一个人，创建对象，访问该对象
        Person son = new Person();
        son.Setperson("小明",19,172,"123456");
        son.eat();
        son.print();
    }
}
