/**
 * Created with IntelliJ IDEA.
 * Description: 日期类
 * User: 何华树
 * Date: 2024-05-17
 * Time: 20:12
 */

// this 引用
    class Date1 {
    public int year;
    public int month;
    public int day;

    public void SetDate(int year,int month,int day) {
//        year = year;
//        month = month;
//        day = day;
        //错误代码： 当函数的形参变量（局部变量） 与 成员变量的名字，相同时，这个代码就没有实现 给 成员变量赋值的 功能
        // 这样的代码根本没有把 成员变量 给赋值，而是 局部变量自己给自己赋值，出了函数，局部变量就被销毁了
        // 成员变量 没有被赋值,当你打印成员变量时，他们是 默认值:0。

//        解决方法？  ---> 加入 this 关键字即可
        this.year = year;
        this.month = month;
        this.day = day;
        this.print();  //可以通过 this 访问当前对象的 非静态 成员方法
    }
    public void print() {
        System.out.println(this.year+" 年 "+this.month+" 月 "+this.day+" 日");
    }
    public Date1() {
        this(2009,9,9); // 这里的 this 表示的是 Date1
        System.out.println("不带参数的构造方法。。。。");
    }
    public Date1(int year,int month,int day) {
        this.year = year;
        this.month = month;
        this.day = day;
        System.out.println("带有3个参数的构造方法。。。。");
    }
}
public class test2 {  //public 修饰的类，名字要与 文件名 相同
    public static void main1(String[] args) {
        Date1 date = new Date1(); //对象的实例化
        date.SetDate(2008,8,8);
        //date.print();

        Date1 date1 = new Date1(); //对象的实例化
        date1.SetDate(2009,9,9);
       // date1.print();


        //问题：这么多对象，调用了同一个 SetDate 方法，那么，在这个方法里面
        // 编译器又是如何区分，是那个对象调用了 SetDate 方法中的 year，month，day的呢？
        //      答：this 关键字

        //那么？this关键字 指的到底是什么呢？  ---> 当前对象的引用
        //问： 什么是当前对象呢？ ---> 那个对象调用了 方法，那这个对象，就是 当前对象。

        //举例：
        Date1 date2 = new Date1(); //对象的实例化
        date2.SetDate(2010,10,10);
        //date1.print();
        // date2,这是一个引用，由该引用，创建了一个新的对象，对象通过引用，访问了 SetDate 方法
        // 那么，这个对象 就是当前对象，而 this 指的就是 date2 这个引用



        // 总结：
        //  1.可以通过 this 访问当前对象的 成员变量 如：
        //  System.out.println(this.year+" 年 "+this.month+" 月 "+this.day+" 日");

        //  2.可以通过 this 访问当前对象的 非静态 成员方法
        //  如：this.print();

        //  3.可以通过 this 访问当前对象的 其他构造方法
        // 什么是构造方法呢？  看下面
    }

    public static void main2(String[] args) {
        // 对象的构造 和 初始化
        //  构造方法 ：
        //  1.无返回值（不能写返回值，void也不可以写）
        //  2.方法名 必须和 类名相同
        //  3.可以 发生重载

        //代码 写两个构造方法，一个带参数，一个不带参数的
        Date1 date = new Date1();  //Date1(),可以 执行 刚刚写的 构造方法
        Date1 date1 = new Date1(2008,9,8);//Date1(2008,9,8) 可以 执行 刚刚写的 构造方法
    }

    public static void main(String[] args) {
        //构造方法的作用： 用来 初始化 对象当中的 成员变量

        //this 总结的第三点： 可以通过 this 访问当前对象的 其他构造方法
        Date1 date = new Date1();

        //结论：当你没有写 任何构造方法的 情况下，java会自动给你提供一个 不带参数的 构造方法
        // 而一旦你 写了构造方法，系统就不会给你提供构造方法了
    }
}


