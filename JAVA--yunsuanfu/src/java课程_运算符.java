/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-04-26
 * Time: 22:16
 */
public class java课程_运算符 {
    public static void main1(String[] args) {
        System.out.println(5/0);//会出现算数异常
    }

    public static void main2(String[] args) {
        System.out.println(-10%3);//结果为 -1
    }

    public static void main3(String[] args) {
        System.out.println(12.5/2);//6.25
        System.out.println(12.5%2);//0.5
        //JAVA当中可以对小数进行求余
    }

    public static void main4(String[] args) {
        System.out.println(1 + 20 + 2.0);//结果为：23.0
        //java当中 类型不一样的，会向类型大的进行靠拢（转化）
        //如：int --> long
    }
    //增量运算符
    //+= -= *= /= %= (与c语言相同的用法)
    public static void main5(String[] args) {
        int a = 10;
        int b = 20;
        System.out.println(a += 10);//20
        System.out.println(b -= 20);//0
        //第一个式子 对 a 进行了修改
        System.out.println(a *= 3);//30
        System.out.println(b /= a);//2
        System.out.println(b %= 3);//2
    }

    //关系运算符
    //!= == < > <= >=
    //c语言当中，非0 为 真  0为假
    //java 当中 真为（true） 假为（false）
    //所以，java当中，关系运算符的返回值只有 true（真） 和 false（假）
    public static void main6(String[] args) {
        System.out.println(10 != 20);//true
        System.out.println(10 == 20);//false
    }

    //逻辑运算符
    // &&（逻辑与） ||（逻辑或） ！（逻辑取反） （语法与c语言一样）
    //但是返回值，同上，只有 true 和 false
     public static void main7(String[] args) {
        int a = 10;
        int b = 20;
         System.out.println(a > b && a == 10);//false
         System.out.println(a < b && a == 10);//true
         System.out.println(a < b || a == 10);//true
         System.out.println(a > b || b == 20);//true
         System.out.println(a > b || a == 20);//false
         System.out.println(!(10 == 10));//false
    }

    //代码：运用 10/0 == 0 去引发算数异常，去验证，短路与，短路或
     public static void main8(String[] args) {
        int a = 10;
        int b = 20;

         System.out.println(a > b && 10/0==0);//结果为 false ，不会引发算数异常
         //前面的 a>b 已经不成立，结果为 false了，后面的式子就不运行了
         System.out.println(a < b || 10/0==0);//结果为 true，不会引发算数异常
         //前面的 a>b 已经成立，结果为 true了，后面的式子就不运行了
    }

    //位运算符
    // &（按位与）
    // |（按位或）
    // ^(按位异或) 相同结果为 0，相异结果为 1
    // ~ （按位取反）
    //以上的为运算符，使用方法均与 c语言 中的相同
    public static void main9(String[] args) {
        // << 左移操作符
        // a<<x,相当于 a * 2的x次方倍，x为移动的位数
        System.out.println(10<<2);//结果为：40
        // >> 右移操作符
        // a>>x,相当于 a / 2的x次方倍，x为移动的位数
        System.out.println(20>>2);//结果为：5
    }

    //位操作符的一些特殊用法
     public static void main10(String[] args) {
         System.out.println(10 > 20 & 10 == 10);//false
         System.out.println(10 < 20 & 10 == 10);//true
         //对于 & 有 0 为 0，全 1 为 1 对于上面两个式子，有 false 为 false ，全 true 为 true
         System.out.println(10 > 20 | 10 == 10);//true
         System.out.println(10 > 20 | 10 == 10);//true
         //对于 | 有 1 为 1，全 0 为 0 对于上面两个式子，有 true 为 true ，全false 为 false
    }
    //条件运算符(三目运算符) 表达式1 ? 表达式2 : 表达式3
    //与 c语言 相同的用法
    //但在 java 当中 表达式的说法不同
    //java当中 是：布尔表达式1 ? 布尔表达式2 : 布尔表达式3
    //注意：三目运算符 会返回进行运算的值，必须要有一个变量去接受这个值
     public static void main11(String[] args) {
    int a = 10 >20? 10: 20;
         System.out.println(a);
    }

    //一道有意思的面试题
     public static void main(String[] args) {
        boolean flag = true == true? true == false? false:true: false;
         System.out.println(flag);
    }
}
