package demo4;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-09-05
 * Time: 17:52
 */
class Money implements Cloneable{
    public double money = 9.9;

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
public class Person implements Cloneable{ //这个类想要被克隆，就必须实现 Cloneable 接口
    public String name;
    public int age;
    public Money m = new Money();

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    //由于 clone 是由 protected 修饰的，
    // 所以，它只能在 Object 的子类内 调用，如果想在类外使用，
    //就要 重写 clone()。

    //浅拷贝 的 clone代码格式：
//    @Override
//    protected Object clone() throws CloneNotSupportedException {
//        return super.clone();
//    }


    //深拷贝 的 clone 代码形式。
    @Override
    protected Object clone() throws CloneNotSupportedException {
        Person tmp = (Person) super.clone();
        tmp.m = (Money)this.m.clone();//将 person1的 Money 对象复制一份
        // 给 tmp的m，使tmp的m 指向新复制的那份 Money对象
        return tmp;
    }
}
