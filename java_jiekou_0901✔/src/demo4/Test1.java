package demo4;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-09-05
 * Time: 17:39
 */
class MyValue {
    public int val;
}
public class Test1 {
    public static void swap(MyValue Val1,MyValue Val2) {
        int tmp = Val1.val;
        Val1.val = Val2.val;
        Val2.val = tmp;
    }
    //这样的方式 是交换不了两个数的值的。
    public static void swap(int x,int y) {
        int tmp = x;
        x = y;
        y = tmp;
    }
    public static void main2(String[] args) {
        MyValue myValue1 = new MyValue();
        MyValue myValue2 = new MyValue();
        myValue1.val = 10;
        myValue2.val = 20;
        System.out.println("修改前："+myValue1.val +" "+myValue2.val);

        swap(myValue1,myValue2);

        System.out.println("修改后："+myValue1.val +" "+myValue2.val);
    }
    public static void main1(String[] args) {
        int a = 10;
        int b = 20;
        System.out.println("修改前："+a+" "+b);
        swap(a,b);
        System.out.println("修改后："+a+" "+b);
    }
}
