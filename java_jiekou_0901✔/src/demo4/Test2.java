package demo4;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-09-05
 * Time: 17:53
 */
public class Test2 {
    public static void main(String[] args)
            throws CloneNotSupportedException {//处理异常
        Person person1 = new Person("张三",10);
        Person person2 = (Person) person1.clone();
//        System.out.println("这是原版本："+person1.toString());
//        System.out.println("这是克隆版本："+person2.toString());
        System.out.println("修改之前："+person1.m.money);
        System.out.println("修改之前："+person2.m.money);
        person2.m.money = 99.99;
        System.out.println("修改之后："+person1.m.money);
        System.out.println("修改之后："+person2.m.money);
    }
}
