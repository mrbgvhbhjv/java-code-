package demo4;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-09-02
 * Time: 19:13
 */
class OutClass {
    public int a = 1;
    public static int b = 2;
    public static final int c = 3;
    static class InnerClass {//静态内部类
        public int d = 4;
        public static int e = 5;
        public static final int f = 6;

        public void test() {
            System.out.println("内部类的test()方法...");
            System.out.println(OutClass.b);
            System.out.println(OutClass.c);
            System.out.println();
        }
    }
}
public class Test {
    public static void main(String[] args) {
        //静态内部类的实例化
        OutClass.InnerClass innerClass = new OutClass.InnerClass();

        //静态内部类：不能直接访问 非静态的 成员变量或者成员方法
        innerClass.test();
    }
}
