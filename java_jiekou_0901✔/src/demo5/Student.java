package demo5;

import java.util.Objects;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-09-05
 * Time: 11:54
 */
public class Student implements Comparable<Student>{
    public String name;
    public int age;

    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return age == student.age && Objects.equals(name, student.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, age);
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    @Override
    public int compareTo(Student o) {
        //根据年龄进行比较
        //最普通写法：
//        if(this.age > o.age) {
//            return 1;
//        } else if (this.age == o.age) {
//            return 0;
//        }else {
//            return -1;
//        }
        //简洁写法：
        return this.age - o.age;

        //根据姓名（字符串）进行比较
//        return this.name.compareTo(o.name);
        //这里返回的是两个字符串的第一个字母的ASCII码的差值。
    }
}
