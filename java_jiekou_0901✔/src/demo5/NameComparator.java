package demo5;

import java.util.Comparator;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-09-05
 * Time: 13:35
 */
public class NameComparator implements Comparator<Student> {
    public int compare(Student o1,Student o2) {
        return o1.name.compareTo(o2.name);
    }
}
