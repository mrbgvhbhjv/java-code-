package demo5;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-09-05
 * Time: 11:54
 */
public class Test {
    public static void mySort(Comparable[] comparables) {

        for (int i = 0; i < comparables.length-1; i++) {

            for (int j = 0; j < comparables.length-1-i; j++) {

                if((comparables[j].compareTo(comparables[j+1])) > 0 ) { //按照升序

                    Comparable temp = comparables[j];
                    comparables[j] = comparables[j+1];
                    comparables[j+1] = temp;

                }
            }
        }
    }
    public static void main1(String[] args) {
        Student student1 = new Student("zhangsan",10);
        Student student2 = new Student("lisi",19);
        //如果要比较两个对象是否相等，怎么做？ -> 用 equals。
//        System.out.println(student1.equals(student2));//false

        //如果要比较两个对象的大小，怎么比较呢？
//        System.out.println(student1 < student2); (错误)
        // -> 实现Comparable接口，并使用compareTo方法
        System.out.println(student1.compareTo(student2));
    }

    public static void main2(String[] args) {
        //如果是多个学生类，例如数组，又该怎么比较？
        Student[] students = new Student[3];
        students[0] = new Student("zhangsan",10);
        students[1] = new Student("lisi",19);
        students[2] = new Student("abc",12);

        System.out.println("排序前："+Arrays.toString(students));

        //模拟实现 mySort(),实现 Arrays.sort 的工作。
//        mySort(students);
        NameComparator nameComparator = new NameComparator();
        AgeComparator ageComparator = new AgeComparator();
        Arrays.sort(students,ageComparator);
        //排序方式，是按照你 在 Student类内 重写的 compareTo 进行排序的

        System.out.println("排序后："+Arrays.toString(students));

    }
}
