package demo3;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-09-02
 * Time: 12:40
 */
public class Fish extends Animal implements ISwimming{
    public Fish(String name, int age) {
        super(name, age);
    }

    @Override
    public void eat() {
        System.out.println("吃鱼粮...");
    }

    @Override
    public void swim() {
        System.out.println(this.name+" 正在水里轻快的游泳。");
    }
}
