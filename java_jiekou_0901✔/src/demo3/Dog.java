package demo3;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-09-02
 * Time: 12:40
 */
public class Dog extends Animal implements IRunning,ISwimming{

    public Dog(String name, int age) {
        super(name, age);
    }

    @Override
    public void eat() {
        System.out.println("吃狗粮...");
    }

    @Override
    public void run() {
        System.out.println(this.name+"正在路上开心的奔跑.");
    }

    @Override
    public void swim() {
        System.out.println(this.name+"正在水里开心的游着泳.");
    }
}
