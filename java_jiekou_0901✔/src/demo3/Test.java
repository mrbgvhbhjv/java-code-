package demo3;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-09-02
 * Time: 12:40
 */
public class Test {
    public static void func(Animal animal) {
        animal.eat();
    }
    public static void flyfunc(IFly iFly) {
        iFly.fly();
    }
    public static void swimfunc(ISwimming iSwimming) {
        iSwimming.swim();
    }
    public static void walk(IRunning iRunning) {
        iRunning.run();
    }

    public static void main(String[] args) {

        robot robot1 = new robot();
        walk(robot1);
        //接口是一种行为规范，只要你满足这种规范，你就可以调用改接口
        //只要你满足接口的规范，就可以实现接口，并调用，实现多态或者其他。
        //无论你是什么 类
    }
    public static void main1(String[] args) {
        //接口的出现，解决了java不能多继承的问题。
        Dog dog = new Dog("旺财",10);
        func(dog);
        walk(dog);
        swimfunc(dog);
        System.out.println("============");
        Duck duck = new Duck("唐老鸭",10);
        func(duck);
        swimfunc(duck);
        walk(duck);
        flyfunc(duck);
        System.out.println("================");
        Fish fish = new Fish("小彩",10);
        func(fish);
        swimfunc(fish);


    }
}
