package demo3;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-09-02
 * Time: 12:40
 */
public class Duck extends Animal implements ISwimming,IRunning,IFly{
    public Duck(String name, int age) {
        super(name, age);
    }

    public void eat() {
        System.out.println("吃鸭粮...");
    }

    @Override
    public void fly() {
        System.out.println(this.name+"张开翅膀在飞。");
    }

    @Override
    public void run() {
        System.out.println(this.name+" 正在慢悠悠的散步。");
    }

    @Override
    public void swim() {
        System.out.println(this.name+" 正在水里轻轻快的游来游去。");
    }
}
