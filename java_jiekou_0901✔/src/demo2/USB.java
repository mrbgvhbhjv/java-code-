package demo2;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-09-01
 * Time: 21:49
 */
public interface USB {
    void OpenDevice();
    void CloseDevice();
}
