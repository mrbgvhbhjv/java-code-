package demo2;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-09-01
 * Time: 21:49
 */
public class Mouse implements USB{

    @Override
    public void OpenDevice() {
        System.out.println("打开设备");
    }

    @Override
    public void CloseDevice() {
        System.out.println("关闭设备");
    }
    public void click() {
        System.out.println("鼠标点击");
    }
}
