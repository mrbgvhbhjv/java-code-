package demo2;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-09-01
 * Time: 21:50
 */
public class Keyboard implements USB{
    @Override
    public void OpenDevice() {
        System.out.println("打开设备");
    }

    @Override
    public void CloseDevice() {
        System.out.println("关闭设备");
    }
    public void input() {
        System.out.println("键盘输入");
    }
}
