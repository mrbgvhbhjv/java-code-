package demo2;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-09-01
 * Time: 21:50
 */
public class Test {
    public static void main(String[] args) {
        Computer computer = new Computer();
        computer.Open();

        computer.useUSBdevice(new Mouse());
        computer.useUSBdevice(new Keyboard());
        computer.Close();
    }
}
