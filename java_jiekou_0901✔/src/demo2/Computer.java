package demo2;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-09-01
 * Time: 21:49
 */
public class Computer {
    public void Open() {
        System.out.println("开机");
    }
    public void Close() {
        System.out.println("关机");
    }
    public void useUSBdevice(USB usb) {
//        if(usb instanceof Mouse) {
//            Mouse mouse = (Mouse)usb;
//            mouse.OpenDevice();
//            mouse.click();
//            mouse.CloseDevice();
//        } else if (usb instanceof Keyboard) {
//            Keyboard keyboard = (Keyboard) usb;
//            keyboard.OpenDevice();
//            keyboard.input();
//            keyboard.CloseDevice();
//        }
        usb.OpenDevice();
        if(usb instanceof Mouse) {
            Mouse mouse = (Mouse)usb;
            mouse.click();
        } else if (usb instanceof Keyboard) {
            Keyboard keyboard = (Keyboard) usb;
            keyboard.input();
        }
        usb.CloseDevice();

    }
}
