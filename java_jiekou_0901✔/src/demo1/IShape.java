package demo1;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-09-01
 * Time: 21:42
 */
public interface IShape {
    void draw();
}
