package demo1;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-09-01
 * Time: 21:42
 */
public class Test {
    public static void drawMap(IShape iShape) {
        iShape.draw();
    }
    public static void main(String[] args) {
        Cycle cycle = new Cycle();
        Triangle triangle = new Triangle();
        drawMap(cycle);
        drawMap(triangle);
    }
}
