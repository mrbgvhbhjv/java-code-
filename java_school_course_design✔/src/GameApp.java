import demo1.GameJFrame;
import demo1.LoginJFrame;
import demo1.RegisterJFrame;

public class GameApp {
    public static void main(String[] args) {
        // 表示程序运行启动的入口


        //如果我们想要打开一个界面，就直接创建哪个界面的对象就可以了
        //new LoginJFrame();

        new GameJFrame();
        //new RegisterJFrame();
    }
}
