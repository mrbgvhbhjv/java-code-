package demo2;


import javax.swing.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class MyJFrame2 extends JFrame implements MouseListener {

    JButton jButton = new JButton("点我啊");

    public MyJFrame2() {
        //设置界面的宽高属性
        this.setSize(603,680);
        //设置界面的标题
        this.setTitle("拼图单机版 v1.0");
        //设置界面置顶
        this.setAlwaysOnTop(true);
        //设置界面居中
        this.setLocationRelativeTo(null);
        //设置关闭模式
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        //取消默认的居中位置，只有取消了，才会按照XY轴的形式添加组件
        this.setLayout(null);

        //给按钮设置宽和高
        jButton.setBounds(0,0,100,50);
        //给按钮绑定鼠标事件
        jButton.addMouseListener(this);
        //可以把 this 看作条件，做什么就会调用对应的方法，this就是鼠标四个触发项


        //把按钮添加到整个界面当中
        this.getContentPane().add(jButton);


        // 让界面显示出来，默认写在最后
        this.setVisible(true);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        System.out.println("单击");
    }

    @Override
    public void mousePressed(MouseEvent e) {
        System.out.println("按下不松开");
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        System.out.println("松开");
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        System.out.println("划入");
    }

    @Override
    public void mouseExited(MouseEvent e) {
        System.out.println("划出");
    }
}
