package demo2;


import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

public class MyJFrame extends JFrame implements ActionListener {

    //创建一个按钮对象
    JButton jButton1 = new JButton("点我啊");
    //创建一个按钮对象
    JButton jButton2 = new JButton("再点我啊");

    public MyJFrame()  {
        //设置界面的宽高属性
        this.setSize(603,680);
        //设置界面的标题
        this.setTitle("拼图单机版 v1.0");
        //设置界面置顶
        this.setAlwaysOnTop(true);
        //设置界面居中
        this.setLocationRelativeTo(null);
        //设置关闭模式
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        //取消默认的居中位置，只有取消了，才会按照XY轴的形式添加组件
        this.setLayout(null);


        //给按钮设置宽和高
        jButton1.setBounds(0,0,100,50);
        //给按钮添加事件
        jButton1.addActionListener(this);
        //this 是表示的是本类里面，对应的代码，就是 actionPerformed() 方法

        //给按钮设置宽和高
        jButton2.setBounds(100,0,100,50);
        //给按钮添加事件
        jButton2.addActionListener(this);
        //this 是表示的是本类里面，对应的代码，就是 actionPerformed() 方法


        //把按钮添加到整个界面当中
        this.getContentPane().add(jButton1);
        this.getContentPane().add(jButton2);


        // 让界面显示出来，默认写在最后
        this.setVisible(true);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        // 对当前按钮进行判断

        //获取当前被操作的那个按钮对象
        Object source = e.getSource();


        //按钮进行比较，看是那个按钮被点击了
        if(source == jButton1) {
            jButton1.setSize(200,200);
        }else if (source == jButton2) {
            Random random = new Random();
            jButton2.setLocation(random.nextInt(500),random.nextInt(500));
        }
    }
}
