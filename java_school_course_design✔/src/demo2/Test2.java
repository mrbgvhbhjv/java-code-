package demo2;


import javax.swing.*;
import java.awt.event.ActionEvent;

public class Test2 {
    public static void main(String[] args) {
        JFrame jFrame = new JFrame();
        //设置界面的宽高属性
        jFrame.setSize(603,680);
        //设置界面的标题
        jFrame.setTitle("拼图单机版 v1.0");
        //设置界面置顶
        jFrame.setAlwaysOnTop(true);
        //设置界面居中
        jFrame.setLocationRelativeTo(null);
        //设置关闭模式
        jFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        //取消默认的居中位置，只有取消了，才会按照XY轴的形式添加组件
        jFrame.setLayout(null);

        //创建一个按钮对象
        JButton jButton = new JButton("点我啊~");
        //设置位置和宽高
        jButton.setBounds(0,0,100,50);

        //给按钮添加动作监听
        //jButton：组件对象，表示你要给哪个组件添加事件
        //addActionListener：表示我要给组件添加哪个事件监听（动作监听包含鼠标左键点击，空格）
        //括号里的参数，表示事件触发之后，要执行的代码
        jButton.addActionListener(new AbstractAction() {
            //当一个接口的实现类，只用到一次的时候，此时，使用匿名内部类，更合适，不用再单独创建一个类了
            //这个匿名内部类，是对于 AbstractAction接口 的实现类，括号里面，才是这个实现类的对象
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("不要点击我");
            }
        });


        //把按钮添加到界面当中


        // 让界面显示出来，默认写在最后
        jFrame.setVisible(true);

    }
}
