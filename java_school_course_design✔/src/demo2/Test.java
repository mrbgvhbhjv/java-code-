package demo2;


import java.util.Random;

public class Test {
    public static void main(String[] args) {
        //需求：
        //创建一维数组，打乱里面的数据的顺序，并输出
        //创建二维数组，将一维数组里面的数据，每四个，放二维数组

        //1.创建一维数组
        int[] ArrayNums = new int[]{0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};

        //2.打乱一维数组中的数据
        Random random = new Random();

        for (int i = 0; i < ArrayNums.length; i++) {
            //获取到随机索引
            //限制随机数的数字，在 0~ArrayNums.length 这个范围里面，且index不会有重复
            int index = random.nextInt(ArrayNums.length);
            //实现数据交换
            int tmp = ArrayNums[i];
            ArrayNums[i] = ArrayNums[index];
            ArrayNums[index] = tmp;
        }

        //遍历一维数组，查看是否交换成功
        for (int i = 0; i < ArrayNums.length; i++) {
            System.out.print(ArrayNums[i] + " ");
        }
        System.out.println();

        //3.创建一个二维数组
        int[][] data = new int[4][4];

        //4.将一维数组的数据放入二维数组
        int count = 0;
        for (int i = 0; i < data.length; i++) {
            for (int j = 0; j < data[i].length; j++) {
                data[i][j] = ArrayNums[count];
                //每添加一个元素，就加1，获取下一个元素的下标
                count++;
            }
        }

        //遍历二维数组
        for (int i = 0; i < data.length; i++) {
            for (int j = 0; j < data[i].length; j++) {
                System.out.print(data[i][j] + " ");
            }
            System.out.println();
        }
    }
}
