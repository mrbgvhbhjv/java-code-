package demo1;


import javax.swing.*;
import javax.swing.border.BevelBorder;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Random;

public class GameJFrame extends JFrame implements KeyListener, ActionListener {
    // JFrame，是用来描述界面的
    //所以，GameJFrame类，作为JFrame类的子类，就可以直接使用 JFrame里面有的方法，来描述游戏界面
    // 所以 GameJFrame类 里面，就是整个游戏界面的逻辑实现，就是游戏的主界面

    //创建一个二维数组（创建为全局成员，是因为initdata() 和 initImage() 都需要用到二维数组）
    int[][] data = new int[4][4];

    //记录空白方块在二维数组中的位置
    int x = 0;
    int y = 0;

    //定义一个变量，记录当前展示图片的路径
    int index = 1;
    String path = "./image\\animal\\animal"+index+"\\";

    //判断胜利，定义一个二维数组，存储正确的数据
    int[][] win = {
            {1,2,3,4},
            {5,6,7,8},
            {9,10,11,12},
            {13,14,15,0}
    };

    //定义变量，统计步数
    int step = 0;

    //创建选项下面的条目对象
    JMenuItem girl = new JMenuItem("美女");
    JMenuItem animal = new JMenuItem("动物");
    JMenuItem sport = new JMenuItem("运动");

    JMenuItem replayItem = new JMenuItem("重新游戏");
    JMenuItem reLoginItem = new JMenuItem("重新登录");
    JMenuItem closeItem = new JMenuItem("关闭游戏");


    public GameJFrame() {
        //初始化界面
        initJFrame();

        //初始化菜单
        initJMenuBar();

        //初始化数据（打乱）
        initdata();

        //初始化图片（根据打乱之后的结果去加载图片）
        initImage();
        // 让界面显示出来，默认写在最后
        this.setVisible(true);
    }

    //初始化数据（打乱数据）
    private void initdata() {
        //1.创建一维数组
        int[] ArrayNums = new int[]{0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};

        //2.打乱一维数组中的数据
        Random random = new Random();

        for (int i = 0; i < ArrayNums.length; i++) {
            //获取到随机索引
            //限制随机数的数字，在 0~ArrayNums.length 这个范围里面，且index不会有重复
            int index = random.nextInt(ArrayNums.length);
            //实现数据交换
            int tmp = ArrayNums[i];
            ArrayNums[i] = ArrayNums[index];
            ArrayNums[index] = tmp;
        }


        //3.将一维数组的数据放入二维数组
        //循环遍历二维数组的每个元素，并给每个元素赋值
        int count = 0;
        for (int i = 0; i < data.length; i++) {
            for (int j = 0; j < data[i].length; j++) {
                //记录空白字符的位置数据
                if (ArrayNums[count] == 0) {
                    x = count / 4;
                    y = count % 4;
                }
                data[i][j] = ArrayNums[count];
                //每添加一个元素，就加1，获取下一个元素的下标
                count++;
            }
        }
    }

    //初始化图片
    //添加图片的时候，就需要按照二维数组中管理的数据来添加图片了
    private void initImage() {
        //清空原本已经出现的所有图片
        this.getContentPane().removeAll();

        if(victory()) {
            //显示胜利的图标
            JLabel winJLabel = new JLabel(new ImageIcon("./image\\win.png"));
            winJLabel.setBounds(203,283,197,73);
            this.getContentPane().add(winJLabel);
        }


        //添加步数图标
        JLabel stepCount = new JLabel("步数：" + step);
        stepCount.setBounds(50,30,100,20);
        this.getContentPane().add(stepCount);



        //细节：
        //先加载的图片会在显示界面的上方，后加载的图片会在显示界面的下方
        //外循环 --- 把内循环重复执行了 4 次。
        for (int i = 0; i < 4; i++) {
            //内循环 -- 表示在一行添加 4 张图片
            for (int j = 0; j < 4; j++) { // 控制行的循环次数
                //获取当前要加载的图片前缀的号码
                int number = data[i][j];
                //创建要给JLabel的对象（管理容器），并创建一个图片 ImageIcon 对象
                JLabel jLabel = new JLabel(new ImageIcon(path + number+".jpg"));
                //指定图片在界面中的位置
                jLabel.setBounds(105 * j + 83,105 * i + 134,105,105);
                //给图片添加边框
                jLabel.setBorder(new BevelBorder(BevelBorder.LOWERED));
                //把管理容器添加到界面当中
                this.getContentPane().add(jLabel);

            }
        }

        //添加背景图片
        JLabel background = new JLabel(new ImageIcon("./image\\background.png"));
        background.setBounds(40,40,508,560);
        //将背景图片添加到界面当中
        this.getContentPane().add(background);


        //刷新界面
        this.getContentPane().repaint();



    }

    private void initJMenuBar() {
        //创建整个的菜单对象
        JMenuBar jMenuBar = new JMenuBar();
        //创建菜单上面的对象 （功能 ）
        JMenu functionJMenu = new JMenu("功能");


        //创建更换图片
        JMenu changeImage = new JMenu("更换图片");

        //将每一个选项下面的条目天极爱到选项当中
        functionJMenu.add(changeImage);
        functionJMenu.add(replayItem);
        functionJMenu.add(reLoginItem);
        functionJMenu.add(closeItem);


        //把美女，动物，运动添加到更换图片当中
        changeImage.add(girl);
        changeImage.add(animal);
        changeImage.add(sport);

        //给条目绑定事件
        replayItem.addActionListener(this);
        reLoginItem.addActionListener(this);
        closeItem.addActionListener(this);
        girl.addActionListener(this);
        animal.addActionListener(this);
        sport.addActionListener(this);

        //将功能菜单添加到菜单当中
        jMenuBar.add(functionJMenu);


        //给整个界面设置菜单
        this.setJMenuBar(jMenuBar);
    }

    private void initJFrame() {
        //设置界面的宽高属性
        this.setSize(603,680);
        //设置界面的标题
        this.setTitle("拼图单机版 v1.0");
        //设置界面置顶
        this.setAlwaysOnTop(true);
        //设置界面居中
        this.setLocationRelativeTo(null);
        //设置关闭模式
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        //取消默认的居中位置，只有取消了，才会按照XY轴的形式添加组件
        this.setLayout(null);

        //给整个界面，添加键盘监听事件
        this.addKeyListener(this);
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    //该方法表示，按住键盘不松开时，调用方法
    @Override
    public void keyPressed(KeyEvent e) {

        int code = e.getKeyCode();
        // 65 表示键盘的 a字母
        if (code == 65) {
            // 把界面中所有的图片删除
            this.getContentPane().removeAll();
            //加载第一张完整的图片
            JLabel all = new JLabel(new ImageIcon(path + "all.jpg"));
            all.setBounds(83,134,420,420);
            this.getContentPane().add(all);

            //加载背景图片
            //添加背景图片
            JLabel background = new JLabel(new ImageIcon("./image\\background.png"));
            background.setBounds(40,40,508,560);
            //将背景图片添加到界面当中
            this.getContentPane().add(background);

            //刷新界面
            this.getContentPane().repaint();

        }




    }

    //该方法表示：松开键盘的时候调用这个方法
    @Override
    public void keyReleased(KeyEvent e) {
        // 判断游戏是否胜利，如果胜利，此方法需要直接结束，不能再执行下面的移动代码了
        if(victory()) {
            //结束方法
            return;
        }


        //对上，下，左，右进行判断
        //左：37 上：38 右：39 下：40

        // 这里的上下左右移动，实际是把上下左右相邻的图片，拉到空白方块处

        int code = e.getKeyCode();
        if (code == 37) {
            System.out.println("向左移动");
            if (y == 3) {
                //表示图片空白方块，已经在最右方了，他的右面，没有图片可以继续向左移动了
                return;
            }
            //把空白方块右方的数字往左移动
            data[x][y] = data[x ][y + 1];
            //空白方块右方的数字，赋值成 0 才可以
            data[x][y + 1] = 0;
            //空白方块的横坐标发生变化
            y++;

            //每移动一次，步数计数器就自增一次
            step++;


            //调用方法，按照最新的数字，加载图片（重新初始化图片）
            initImage();
        } else if (code == 38) {
            System.out.println("向上移动");
            if (x == 3) {
                //表示图片空白方块，已经在最下方了，他的下面，没有图片可以继续移动了
                return;
            }
            //逻辑：
            //把空白方块下方的数字，向上移动
            //x，y 表示空白方块
            //x + 1，y 表示空白方块下方的数字

            //把空白方块下方的数字，赋值给空白方块
            data[x][y] = data[x + 1][y];
            //空白方块下方的数字，赋值成 0 才可以
            data[x + 1][y] = 0;
            //空白方块的横坐标发生变化
            x++;

            //每移动一次，步数计数器就自增一次
            step++;


            //调用方法，按照最新的数字，加载图片（重新初始化图片）
            initImage();

        } else if (code == 39) {
            System.out.println("向右移动");
            if (y == 0) {
                //表示图片空白方块，已经在最左方了，他的左面，没有图片可以继续向右移动了
                return;
            }
            //把空白方块左方的数字往右移动
            data[x][y] = data[x ][y - 1];
            //空白方块右方的数字，赋值成 0 才可以
            data[x][y - 1] = 0;
            //空白方块的横坐标发生变化
            y--;

            //每移动一次，步数计数器就自增一次
            step++;


            //调用方法，按照最新的数字，加载图片（重新初始化图片）
            initImage();
        } else if (code == 40) {
            System.out.println("向下移动");
            if (x == 0) {
                //表示图片空白方块，已经在最上方了，他的上面，没有图片可以继续向下移动了
                return;
            }
            //把空白方块上方的数字往下移动
            data[x][y] = data[x - 1][y];
            //空白方块上方的数字，赋值成 0 才可以
            data[x - 1][y] = 0;
            //空白方块的横坐标发生变化
            x--;

            //每移动一次，步数计数器就自增一次
            step++;

            //调用方法，按照最新的数字，加载图片（重新初始化图片）
            initImage();
        }else if (code == 65){
            initImage();
        } else if (code == 87) {
            // 直接获得胜利
            data = new int[][] {
                    {1,2,3,4},
                    {5,6,7,8},
                    {9,10,11,12},
                    {13,14,15,0}
            };
            initImage();
        }
    }


    //判断data数组中的数据，是否跟win数组中的相同
    //如果全部相同，返回 true ，否则返回 false
    public boolean victory() {
        for (int i = 0; i < data.length; i++) {
            for (int j = 0; j < data[i].length; j++) {
                if (data[i][j] != win[i][j]) {
                    //只要有一个数据不一样，就返回false
                    return false;
                }
            }
        }
        //循环结束，表示数组遍历比较完毕，全都一样，才能到达这个语句，返回true
        return true;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        //获取当前被点击的条目的对象
        Object obj = e.getSource();

        //判断
        if (obj == girl) {
            Random random = new Random();
            path = "./image\\girl\\girl"+index+"\\";
            index = random.nextInt(13);

            //思路顺序：先清零，然后打乱二维数组的数据，再加载图片
            //步数计算器清零
            step = 0;
            //再次打乱二维数组中的数据
            initdata();
            //重新加载图片
            initImage();

        } else if (obj == animal) {
            Random random = new Random();
            path = "./image\\animal\\animal"+index+"\\";
            index = random.nextInt(8);

            //思路顺序：先清零，然后打乱二维数组的数据，再加载图片
            //步数计算器清零
            step = 0;
            //再次打乱二维数组中的数据
            initdata();
            //重新加载图片
            initImage();
        } else if (obj == sport) {
            Random random = new Random();
            path = "./image\\sport\\sport"+index+"\\";
            index = random.nextInt(10);

            //思路顺序：先清零，然后打乱二维数组的数据，再加载图片
            //步数计算器清零
            step = 0;
            //再次打乱二维数组中的数据
            initdata();
            //重新加载图片
            initImage();
        }else if(obj == replayItem) {
            System.out.println("重新游戏");

            //思路顺序：先清零，然后打乱二维数组的数据，再加载图片
            //步数计算器清零
            step = 0;
            //再次打乱二维数组中的数据
            initdata();
            //重新加载图片
            initImage();


        } else if (obj == reLoginItem) {
            System.out.println("重新登录");
            //关闭当前的游戏界面
            this.setVisible(false);
            //打开登录界面
            new LoginJFrame();
        } else if (obj == closeItem) {
            System.out.println("关闭游戏");
            //直接关闭java虚拟机即可
            System.exit(0);
        }
    }
}
