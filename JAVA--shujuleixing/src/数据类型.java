/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-04-26
 * Time: 17:52
 */
public class 数据类型 {

    //final 相当于 c语言 中的 const 将一个变量变为常量，固定不能修改
    public static void main1(String[] args) {
        int a = 10;
        a = 20;
        System.out.println(a);
    }
    public static void main2(String[] args) {
        //一个变量没有初始化，使用的时候就会报错
        //int a;//错误代码
        int a = 40;
        System.out.println(a);
    }
    public static void main3(String[] args) {
        //long 长整形数据类型
        //long a = 20;//写的不完全正确

        //在long定义的变量初始化时，为了区分 int 和 long 的区别
        //建议在值的后面加上 ‘L’ 或者 ‘l’，但是更推荐加 ‘L’，因为有时候会把 ‘l’ 看错为 1。
        long a = 30L;
        long b = 40l;
        System.out.println(a);
        System.out.println(b);
    }
    public static void main4(String[] args) {
        //float b = 12.3;//error
        //12.3默认是 double类型，需要在12.3后面加上 F 或者 f 才是float类型
        float a = 12.3f;
        System.out.println(a);
    }
    //数据类型：字符型：char
    //char 占两个字节（与c语言不同，c语言的char只占用1个字节）
    //所以，java里的 char 可以存放汉字
    public static void main5(String[] args) {
        char a = 'a';
        char b = '何';
        char c = '华';
        char d = '树';
        System.out.println(a);
        System.out.println(b);
        System.out.println(c);
        System.out.println(d);
    }
    public static void main6(String[] args) {
        int a = 10;
        long b = 20;
        //a=b;//long --> int  8个字节-->4个字节 存不下
        b = a;//int --> long 4-->8 可以改
        System.out.println(a);
        System.out.println(b);
        //可以使用 c语言 中的 printf 用来打印。
        System.out.printf("%d %d",a,b);
    }

    //强制类型转换（与 c语言 的强制类型转换一样的做法）
    public static void main7(String[] args) {
        //强制类型转换，可能会造成数据丢失
        byte a = 1;
        int b = 128;
        a = (byte)b;// -128
        System.out.println(a);
        //分析：byte 存储整数的范围是：0~127
        //具体原因看 写的笔记
        //
    }

    public static void main8(String[] args) {
        //字符串类型：（c语言当中没有的全新类型）
        String str = "hello";
        System.out.println(str);//hello
    }

    public static void main9(String[] args) {
        String str1 = "hello ";
        String str2 = "world";
        System.out.println(str1 + str2);//hello world
        // ‘+’ 表示拼接的意思，好比如 ：c语言中的字符串函数：strcat（追加字符串的函数）
    }
    //JAVA的字符串当中，没有所谓的‘\0’作为结尾的说法

    public static void main10(String[] args) {
        //访问str1中的第一个字符
        String str1 = "hello";
        System.out.println(str1.charAt(0));
    }

    public static void main11(String[] args) {
        int a = 10;
        System.out.println("a = " +a);// a = 10
        // '+' 表示拼接的作用
        System.out.println("a = " + a + a);//a = 1010
        System.out.println("a = " +(a+a));//a = 20
        //第一个 + 表示拼接，第二个 + 表示计算求和
    }

    public static void main12(String[] args) {
        //java当中有类似于 c语言 的打印
        int a = 10;
        int b = 20;
        System.out.println(a);//10
        System.out.println(b);//20
        System.out.printf("%d %d\n",a,b);//10 20
    }

    public static void main13(String[] args) {
        int a = 10;
        int c = 20;
        System.out.println("a == "+ a + c);//两个都是拼接功能
        System.out.println("a== "+(a+c));//先加后拼接
        System.out.println(a + c +"==a");//第一个是相加运算，第二个是拼接
    }

    public static void main14(String[] args) {
        //整数转换为字符串
        int a = 1234;
        String str =String.valueOf(a);
        System.out.println(str);//1234

        int b = 1234;
        String str1 = a + "";
        System.out.println(str1+6);//12346
        //说明 1234 已经是字符串，使用 ‘+’ 时，+的功能是拼接
        //字符串转换成整数
        String str2 = "1239";
        int s = Integer.valueOf(str2);
        System.out.println(s+1);//1240
    }

}





