import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-04-27
 * Time: 16:01
 */
public class java课程_程序逻辑控制 {
    //练习：判断一个数是奇数还是偶数
    public static void main1(String[] args) {
        int i = 9;
        if(i % 2 == 0) {
            System.out.println("偶数");
        }
        else {
            System.out.println("奇数");
        }
    }

    public static void main2(String[] args) {
        //练习：判断一个数是正数还是负数，还是0

        //输入的标准流程
        Scanner sc = new Scanner(System.in);
        int input = sc.nextInt();
        if(input < 0){
            System.out.println("负数");
        }
        else if(input > 0){
            System.out.println("正数");
        }
        else{
            System.out.println(0);
        }
    }

    public static void main3(String[] args) {
        //练习：判断闰年
        Scanner sc = new Scanner(System.in);
        int year = sc.nextInt();
        if(year % 4 == 0 && year % 100 != 0){
            System.out.println("是闰年");
        }
        else if(year % 400 == 0){
            System.out.println("是闰年");
        }
        else{
            System.out.println("不是闰年");
        }
    }

    public static void main4(String[] args) {
        //练习：输入一个年龄，判断这个年龄处于什么状态
        Scanner sc = new Scanner(System.in);
        int age = sc.nextInt();
        if(age >= 14&&age<= 20){
            System.out.println("青少年");
        }else if(age >= 21 && age <= 40){
            System.out.println("青年");
        }else if(age >= 41 && age <= 60){
            System.out.println("中年");
        }else if(age >= 61 && age <= 80) {
            System.out.println("中老年");
        }else if(age >= 81 && age <= 100){
            System.out.println("老年");
        } else{
            System.out.println("老寿星");
        }
    }

    public static void main5(String[] args) {
        //简单使用switch
        //switch(参数)
        //参数的类型可以是：byte,char,int,short,枚举,字符串
        //不可以的类型是：float,double,boolean,long,复杂表达式

        //输入一个数字，输出对应的星期几
        Scanner sc = new Scanner(System.in);
        int input = sc.nextInt();
        switch(input){
            case 1:
                System.out.println("星期一");
                break;
            case 2:
                System.out.println("星期二");
                break;
            case 3:
                System.out.println("星期三");
                break;
            case 4:
                System.out.println("星期四");
                break;
            case 5:
                System.out.println("星期五");
                break;
            case 6:
                System.out.println("星期六");
                break;
            case 7:
                System.out.println("星期天");
                break;
            default:
                System.out.println("输入错误");
        }
    }

    public static void main(String[] args) {

    }
}
