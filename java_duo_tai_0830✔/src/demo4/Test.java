package demo4;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-08-31
 * Time: 16:52
 */
public class Test {
    public static void DescriptFood(Food food) {
        food.descript();
    }
    public static void main(String[] args) {
        Rice rice = new Rice();
        Noodle noodle = new Noodle();
        Dumpling dumpling = new Dumpling();

        DescriptFood(rice);
        DescriptFood(noodle);
        DescriptFood(dumpling);
    }
}
