package demo4;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-08-31
 * Time: 16:56
 */
public class Rice extends Food{
    @Override
    public void descript() {
        System.out.println("这是大米饭，是中国乃至世界的传统主食。");
    }
}
