package demo4;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-08-31
 * Time: 16:56
 */
public class Dumpling extends Food{
    @Override
    public void descript() {
        System.out.println("这是饺子，是中国北方过年时最喜欢的主食。");
    }
}
