package demo3;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-08-31
 * Time: 16:43
 */
public class Test {
    public static void drawMap(Shape shape) {
        shape.draw();
    }

    public static void main(String[] args) {
        Rect rect = new Rect();
        Cycle cycle = new Cycle();
        Flower flower = new Flower();
        Triangle triangle = new Triangle();

        Shape[] shapes = {rect,cycle,flower,cycle,triangle};

        for (Shape shape:shapes) {
            shape.draw();
        }

        for (int i = 0;i< shapes.length;i++) {
            Shape shape = shapes[i];
            shape.draw();
        }
    }
    public static void main2(String[] args) {
        //如果不使用多态，如何实现类似于多态这样的效果？  -> if else 语句
        //但是，这样的代码 看着就有点难受
        Rect rect = new Rect();
        Cycle cycle = new Cycle();
        Flower flower = new Flower();
        Triangle triangle = new Triangle();

        // 创建了一个 Shape类型的 数组
        String[] strings = {"rect","cycle","flower","cycle","triangle"};

        for (String s:strings) {
            if(s.equals("rect")) {
                rect.draw();
            }else if (s.equals("cycle")) {
                cycle.draw();
            }else if (s.equals("flower")) {
                flower.draw();
            }else {
                triangle.draw();
            }
        }
    }
    public static void main1(String[] args) {
        //典型的多态实现方式。
        Rect rect = new Rect();
        Cycle cycle = new Cycle();
        Flower flower = new Flower();
        Triangle triangle = new Triangle();

        drawMap(rect);
        drawMap(cycle);
        drawMap(flower);
        drawMap(triangle);

    }
}
