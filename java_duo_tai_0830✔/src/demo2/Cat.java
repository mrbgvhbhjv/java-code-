package demo2;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-08-30
 * Time: 21:14
 */
public class Cat extends Animal{
    public Cat(String name, int age) {
        super(name, age);
    }
    public void mimi() {
        System.out.println(this.name+" 咪咪叫...");
    }
    public void eat() {
        System.out.println(this.name+"在吃鱼...");
    }
}
