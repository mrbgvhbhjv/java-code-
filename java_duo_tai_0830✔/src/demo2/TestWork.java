package demo2;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-08-30
 * Time: 19:14
 */
class X {
    {
        Y y = new Y();
    }
    public X() {
        System.out.println("X");
    }
}
class Y {
    public Y() {
        System.out.println("Y");
    }
}
class Z extends X {
    {
        Y y = new Y();

    }
    public Z() {
        System.out.println("Z");
    }
}
public class TestWork {
    public static void main(String[] args) {
        new Z();
    }
}
