package demo2;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-08-30
 * Time: 21:14
 */
public class Test {
    public static void func1(Animal animal) {
        animal.eat();
    }
    public static void main(String[] args) {
        Dog dog = new Dog("旺财",10);
        func1(dog);
        System.out.println("==========");
        Cat cat = new Cat("mimi",9);
        func1(cat);
    }
}
