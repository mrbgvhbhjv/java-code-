package demo2;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-08-30
 * Time: 21:14
 */
public class Dog extends Animal{
    public Dog(String name, int age) {
        super(name, age);
    }
    public void bark() {
        System.out.println(this.name+" 汪汪叫.....");
    }
    public void eat() {
        System.out.println(this.name+"在啃骨头...");
    }
}
