package demo1;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-08-30
 * Time: 17:40
 */
public class Bird extends Animal{
    public Bird(String name, int age) {
        super(name, age);
    }

    public void fly() {
        System.out.println(this.name+" 正在天上飞....");
    }
}
