package demo1;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-08-30
 * Time: 17:40
 */
public class Animal {
    public String name;
    public int age;

    public Animal(String name, int age) {
        this.name = name;
        this.age = age;
    }


    public void eat() {
        System.out.println(this.name+" 在吃饭...");
    }
//    public void bark() {
//        System.out.println(this.name+" 汪汪叫");
//    }
}
