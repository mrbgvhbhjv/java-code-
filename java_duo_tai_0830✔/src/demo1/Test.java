package demo1;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-08-30
 * Time: 17:43
 */
public class Test {
    public static Animal func2() {
        Bird bird = new Bird("彩虹",9);
        System.out.println(bird.name);
        return bird;
    }
    public static void func1(Animal animal1) {
        System.out.println(animal1.name);
        System.out.println("实现了Dog->Animal的向上转型 ");
    }
    public static void main(String[] args) {
        //向上转型的三种方式：
        //1.直接赋值：
        Animal animal = new Dog("旺财",10);
        System.out.println(animal.name);
//        animal.bark(); //向上转型的缺点：无法直接调用子类特有的方法，只能调用父类自己的。


        //2.方法传参：以 Dog 和 func1() 为例子。
        Dog dog = new Dog("小黄",10);
        func1(dog);
        System.out.println(dog.name);

        //3.方法的返回值：以 Bird 和 func2() 为例子。
        Animal animal2 = func2();
        System.out.println(animal2.name);
    }
}
