package demo1;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-08-30
 * Time: 17:40
 */
public class Dog extends Animal{
    public Dog(String name, int age) {
        super(name, age);
    }

    public void bark() {
        System.out.println(this.name+" 在汪汪叫...");
    }
}
