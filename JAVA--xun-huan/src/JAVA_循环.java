import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-05-04
 * Time: 10:52
 */

//
    //JAVA的循环语句

//while（布尔表达式）--括号内必须是带有判断，与c语言不同
//for(表达式1；布尔表达式；表达式3)--与c语言的相同
//do while（布尔表达式）--与c语言的语法相同
public class JAVA_循环 {
    //练习：打印 1~10 的数字（while，for，do while）
    public static void main1(String[] args) {
        int i = 1;
//        while(i<=10){
//            System.out.printf("%d ",i);
//            i++;
//        }
        for(i = 1;i <= 10;i++) {
            System.out.printf("%d ",i);
        }
//        do{
//            System.out.printf("%d ",i);
//            i++;
//        }while(i<=10);
    }

    public static void main2(String[] args) {
        //练习：计算 5 的阶乘（while，for，do while）
        int i = 1;
        int sub = 1;
//        while(i<=5){
//            sub *= i;
//            i++;
//        }
        for (i = 1; i <=5 ; i++) {
            sub *= i;
        }
//        do {
//            sub *= i;
//            i++;
//        }while(i<=5);
        System.out.println(sub);
    }

    public static void main3(String[] args) {
        //练习：计算 1！+ 2！+3！+4！+5！....+n！
        //计算 1！+ 2！+3！+4！+5！（while，for，do while）
        //while写法
        int i = 1;
        int sum = 0;
        while(i <= 5){//控制阶乘的数字
            int j = 1;
            int sub = 1;//每次进来，阶乘都要重新置1；
            while(j <= i){//控制 乘 的次数
                sub *= j;
                j++;
            }
            sum += sub;
            i++;
        }
        System.out.println(sum);
    }

    public static void main4(String[] args) {
        //练习：计算 1！+ 2！+3！+4！+5！....+n！
        //计算 1！+ 2！+3！+4！+5！（while，for，do while）
        //for写法
        int i = 1;
        int sum = 0;
        for(i = 1; i<=5;i++){
            int j = 1;
            int sub = 1;
            for(j=1;j<=i;j++){
                sub *= j;
            }
            sum +=sub;
        }
        System.out.println(sum);
    }

    public static void main5(String[] args) {
        //练习：计算 1！+ 2！+3！+4！+5！....+n！
        //计算 1！+ 2！+3！+4！+5！（while，for，do while）
        //do while写法
        int i = 1;
        int sum = 0;
        do {
            int j= 1;
            int sub = 1;
            do {
                sub *= j;
                j++;
            }while(j<=i);
            sum += sub;
            i++;
        }while(i<=5);
        System.out.println(sum);
    }
    //break,continue 和 c语言学得一模一样
    public static void main6(String[] args) {
        //练习：用循环打印1~10的数字，当数字为5的时候就跳出循环
        //for写法
//        int i = 1;
//        for ( i = 1; i <=10 ; i++) {
//            if(i == 5){
//                break;
//            }
//            System.out.printf("%d ",i);
//        }
        //while写法
//        int i = 1;
//        while(i<=10) {
//            if(i == 5){
//                break;
//            }
//            System.out.println(i);
//            i++;
//        }
        // do while 写法
        int i = 1;
        do {
            if(i==5){
                break;
            }
            System.out.println(i);
            i++;
        }while(i<=10);
    }

    public static void main7(String[] args) {
        //练习：用循环，打印 1~10 的数字，选择性的打印不关于 3 的倍数的数
        //while写法
//        int i = 1;
//        while(i<=10){
//            if(i % 3 == 0){
//                i++;
//                continue;
//            }
//            System.out.println(i);
//            i++;
//        }
        int i = 1;
        for(i = 1;i <= 10;i++){
            if(i % 3 == 0){
                //这里没有i++，是因为for循环中的 continue，跳过的是最后一步i++之前的句子。
                continue;
            }
            System.out.println(i);
        }
    }

    public static void main8(String[] args) {
        //练习：求出 1~100 之间，左右既能被 3 ，也能被 5 整除的数
        int i = 1;
        while(i<=100){
//            if( i % 3 == 0 && i % 5 == 0){
//                System.out.println(i);
//            }
            if(i % 15 == 0){
                System.out.println(i);
            }
            i++;
        }
    }

    public static void main9(String[] args) {
        //判断一个数是不是素数，简单运用一下 java 里面的 求根公式
        Scanner s = new Scanner(System.in);
        int input = s.nextInt();
        int i = 0;
        int flag = 0;
        for(i = 2;i<Math.sqrt(input);i++) {
            flag = 1;
            if( input % i == 0) {
                flag = 0;
                System.out.println(input+"不是素数");
                break;
            }
        }
        if(flag == 1) {
            System.out.println(input + "是素数");
        }
    }

    public static void main10(String[] args) {
        //1~100之间判断 9 出现的次数
        int i = 1;
        int count = 0;
        for(i = 1;i<=100;i++) {
            if(i / 10 == 9) {
                count++;
            }
            if(i % 10 == 9) {
                count++;
            }
        }
        System.out.println(count);
    }

    public static void main11(String[] args) {
        //打印 X 图形 运用多组输入
        Scanner s = new Scanner(System.in);
        while(s.hasNextInt()) {
            int input = s.nextInt();
            int i = 0;
            for(i = 0;i<input;i++) {
                int j = 0;
                for(j = 0;j<input;j++) {
                    if(i == j || i+j == input - 1) {
                        System.out.print("*");
                    }else {
                        System.out.print(" ");
                    }
                }
                System.out.println();
            }
        }
    }
}




















