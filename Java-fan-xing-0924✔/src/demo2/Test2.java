package demo2;

/**
 * Created with IntelliJ IDEA.
 * Description: 泛型的上界的举例
 * User: 何华树
 * Date: 2024-10-08
 * Time: 19:46
 */
// 写一个泛型类，可以求得数组里面的最大值

//class Alg<E> {
//    // 这个想要用来求取最大值的泛型类，有个很大的问题：
//    // 不知道具体要比较的类型
//
//    private Object[] objects = new Object[10];
//
//    public E findMax(E[] array) {
//        E max = array[0];
//        for (int i = 0; i < array.length; i++) {
//            if(max < array[i]) {
//                max = array[i];
//            }
//        }
//        return max;
//    }
//}

class Alg<E extends Comparable<E>> {
    private Object[] objects = new Object[10];

    public E findMax(E[] array) {
        E max = array[0];
        for (int i = 0; i < array.length; i++) {
            if(max.compareTo(array[i]) < 0) {
                max = array[i];
            }
        }
        return max;
    }
}

class Alg2 {
    private Object[] objects = new Object[10];

    public  <E extends Comparable<E>> E findMax(E[] array) {
        E max = array[0];
        for (int i = 0; i < array.length; i++) {
            if(max.compareTo(array[i]) < 0) {
                max = array[i];
            }
        }
        return max;
    }
}

class Alg3 {
    private Object[] objects = new Object[10];

    public static <E extends Comparable<E>> E findMax(E[] array) {
        E max = array[0];
        for (int i = 0; i < array.length; i++) {
            if(max.compareTo(array[i]) < 0) {
                max = array[i];
            }
        }
        return max;
    }
}

class Person implements Comparable<Person> {

    @Override
    public int compareTo(Person o) {
        return 0;
    }
}
public class Test2 {
    public static void main(String[] args) {
        // 静态泛型方法 求取数组里面的最大值。
        Integer[] array = new Integer[]{1,2,3,4,5,6,45,7,8};

        // 通过类名调用了 静态泛型方法 求取最大值。
        int ret = Alg3.<Integer>findMax(array);

        System.out.println(ret);

    }
    public static void main2(String[] args) {
        // 通过类里面的 泛型方法 求取数组里面的最大值
        Integer[] array = new Integer[]{1,2,3,4,5,6,45,7,8};

        Alg2 alg = new Alg2();
//        int ret = alg.findMax(array); // 隐藏式的写法，没有把泛型 写出来
        int ret = alg.<Integer>findMax(array); // 实际的写法
        System.out.println(ret);
    }
    public static void main1(String[] args) {
        // 通过泛型类，求取数组里面的最大值。
        Integer[] array = new Integer[]{1,2,3,4,5,6,45,7,8};

        Alg<Integer> alg1 = new Alg<>();
        int ret = alg1.findMax(array);
        System.out.println(ret);

        Alg<Person> alg2 = new Alg();
    }
}
