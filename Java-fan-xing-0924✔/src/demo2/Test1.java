package demo2;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-10-08
 * Time: 14:32
 */

class MyArray<E> {
    private Object[] objects =new Object[10];

    public void setObject(int pos,E arr) {
        objects[pos] = arr;
    }

    public E getObject(int pos) {
        return (E)objects[pos];
    }
}
public class Test1 {
    public static void main(String[] args) {

    }
    public static void main2(String[] args) {
        MyArray<Integer> myArray1 = new MyArray<Integer>();

        myArray1.setObject(0,10);
        myArray1.setObject(1,20);
        //myArray1.setObject(2,"hello");// 自动类型检查

        int ret = myArray1.getObject(0);
        System.out.println(ret);

        System.out.println();

        MyArray<String> myArray2 = new MyArray<>();
        myArray2.setObject(0,"hello");
        myArray2.setObject(1,"world");

        String str1 = myArray2.getObject(0);
        String str2 = myArray2.getObject(1);
        System.out.println(str1+str2);
        System.out.println(myArray2.getObject(0) + myArray2.getObject(1));


    }
    public static void main1(String[] args) {
        MyArray myArray = new MyArray();
        myArray.setObject(0,10);
        myArray.setObject(1,20);
        myArray.setObject(2,"hello");

        int ret = (int)myArray.getObject(0);

        String retstr = (String)myArray.getObject(2);

    }
}
