package demo1;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-09-24
 * Time: 13:40
 */
//class MyArray {
//    public Object[] array = new Object[]{10};
//
//    public Object getArray(int pos) {
//        return array[pos];
//    }
//
//    public void setArray(int pos,Object arr) {
//        array[pos] = arr;
//    }
//}


class MyArray<E> {
    public Object[] array = new Object[10];

    public E getArray(int pos) {
        return (E)array[pos];
    }

    public void setArray(int pos,E arr) {
        array[pos] = arr;
    }
}

class MyArray2<E extends Number> {
    public Object[] array = new Object[10];

    public E getArray(int pos) {
        return (E)array[pos];
    }

    public void setArray(int pos,E arr) {
        array[pos] = arr;
    }
}
public class Test1 {
    public static void main6(String[] args) {
        //6.泛型的上界
        MyArray2<Integer> myArray1 = new MyArray2<>();

        //因为此时的泛型类，指定了 传入的类类型，必须是 Number 或是 Number的子类
        // String 不是Number的子类，所以，不能传入 String 。
        //MyArray2<String> myArray2 = new MyArray2<String>();

    }

    public static void main5(String[] args) {
        //5.擦除机制

        // 假如我要打印 Test1 类型的对象的地址
        Test1 test1 = new Test1();
        System.out.println(test1);

        // 同理，打印 MyArray 类 的实例化 对象的地址
        MyArray<Integer> myArray1 = new MyArray<>();
        MyArray<String> myArray2 = new MyArray<>();

        System.out.println(myArray1);
        System.out.println(myArray2);

        //发现，打印的形式是一样，并没有如我们想像一样，把 <> 里面的也打印出来
        // 其实就是 擦除机制的作用，被擦除为 Object
        // 泛型 ——》不参与类型的构成。
    }
    public static void main4(String[] args) {
        // 4.裸泛型
        MyArray myArray = new MyArray();
        // 没有写 <> ，这就是一个裸泛型，是为了兼容以前的版本
        myArray.setArray(0,10);
        myArray.setArray(1,"hello");

        Integer a = (Integer) myArray.getArray(1);
        System.out.println(a);
        // 这里输出结果，会报异常，名为：类转换异常

        // 所以，裸泛型会有问题，所以，不要写这样的代码
        // 一定要把 <> 给加上。
    }
    public static void main3(String[] args) {
        //使用 泛型
        MyArray<Integer> myArray = new MyArray<Integer>();
        //让 myArray 这个对象里面，只能存放 整形类型的 数据

        myArray.setArray(0,10);
//          myArray.setArray(1,"jfdakjs");
        //泛型完成的第一个工作：自动类型检查

        myArray.setArray(1,20);

        int ret1 = myArray.getArray(0);
        int ret2 = myArray.getArray(1);
        //泛型完成的第二个工作：自动类型转换



    }
    public static void main2(String[] args) {
        MyArray myArray = new MyArray();
        myArray.setArray(0,10);
        myArray.setArray(1,"hello");

        String str = (String)myArray.getArray(1);
        int ret = (int)myArray.getArray(0);

        //有问题：1.确实是可以存放任何数据了，但是，太乱了！
        //      2.当我去获取数据的时候，由于get方法的返回值是Object,
        //    每一次去获取数据，我都要去看我获取的是什么数据，然后强制类型转换，这样子，太麻烦了

        // 待解决的两个问题
        //1. 数据类型自动检查 问题
        //2. 数据自动类型转换 问题
    }
    public static void main1(String[] args) {
        //装箱
        int a = 10;
        Integer i = Integer.valueOf(a); // 手动装箱，当然，可以简化
        Integer i1 = a; // 手动装箱，简化版本
        Integer i2 = 10; //自动装箱，由编译器自动转化

        //拆箱
        Integer b = 20;
        //自动拆箱
        int c = b;
        int c1 = (int)b;

        //手动拆箱
        int c2 = b.intValue();
    }
}
