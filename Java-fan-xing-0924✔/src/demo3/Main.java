package demo3;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-10-21
 * Time: 10:41
 */
class MyArray<E> {
    private Object[] objects = new Object[10];

    public E get(int pos) {
        return (E)objects[pos];
    }

    public void set(int pos,E object) {
        objects[pos] = object;
    }
}

class findMax<E extends Comparable<E>> {
    public E FindMax(E[] array) {
        E max = array[0];
        for (int i = 1; i < array.length; i++) {
            if(max.compareTo(array[i]) < 0) {
                E temp = max;
                max = array[i];
                array[i] = temp;
            }
        }
        return max;
    }
}
public class Main {
    public static  <E extends Comparable<E>> E FindMax(E[] array) {
        E max = array[0];
        for (int i = 1; i < array.length; i++) {
            if(max.compareTo(array[i]) < 0) {
                E temp = max;
                max = array[i];
                array[i] = temp;
            }
        }
        return max;
    }

    public static void main(String[] args) {
        Integer[] intarr = new Integer[]{1,2,3,4,5,6,7,8};
        int ret = FindMax(intarr);
        System.out.println(ret);
    }
    public static void main4(String[] args) {
        Integer[] intarr = new Integer[]{1,2,3,4,5,6,7,8};
        Main main = new Main();
        int ret = main.<Integer>FindMax(intarr);
        System.out.println(ret);
    }
    public static void main3(String[] args) {
        Integer[] myArray1 = new Integer[]{1,2,3,4,5,6,7};
        findMax<Integer> findMax = new findMax<>();
        int ret = findMax.FindMax(myArray1);
        System.out.println(ret);

        findMax<String> findMax1 = new findMax<>();
        String[] str = new String[]{"abc","bcd","cdf","ayx"};
        String retstr = findMax1.FindMax(str);
        System.out.println(retstr);
    }
    public static void main2(String[] args) {
        MyArray<Integer> myArray1 = new MyArray<>();
        myArray1.set(0,10);
        myArray1.set(1,20);
        //myArray1.set(2,"djsfi");

        int ret = myArray1.get(0);
        System.out.println(ret);

        MyArray<String> myArray = new MyArray<>();
        myArray.set(0,"hello ");
        myArray.set(1," world");
        //myArray.set(2,10);

        String retstr = myArray.get(1);
        String rets = myArray.get(0);

        System.out.println(rets + retstr);
    }
    public static void main1(String[] args) {
        MyArray myArray = new MyArray();
        myArray.set(0,10);
        myArray.set(1,"hsh");

        int ret1 = (int)myArray.get(0);
        String ret2 = (String)myArray.get(1);

        System.out.println(ret1 + ret2);
    }
}
