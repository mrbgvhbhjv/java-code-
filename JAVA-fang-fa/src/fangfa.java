import java.net.SecureCacheResponse;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-05-09
 * Time: 21:32
 */


//            方法：实际上与 C语言 的函数差不多
    //方法的格式：
    // public static 返回值 方法名 （形式参数列表） ｛
    //        方法体
    //   ｝
    //
public class fangfa {
    public static boolean isLeapyear(int a) {
        if((a % 4 == 0 && a % 100 != 0) || a % 400 == 0) {
            return true;
        }else {
            return false;
        }
    }
    public static void main1(String[] args) {
        //写一个方法：判断输入的年份是否为闰年
        Scanner s = new Scanner(System.in);
        int year = s.nextInt();
        boolean i = isLeapyear(year);
        if(i==true) {
            System.out.println("闰年");
        }else {
            System.out.println("平年");
        }
    }

    public static int add(int a,int b) {
        return a + b;
    }
    public static void main2(String[] args) {
        //写一个方法：实现两个数进行相加
        Scanner s = new Scanner(System.in);
        int a = s.nextInt();
        int b = s.nextInt();
        System.out.println(add(a,b));
    }

    public static int func(){
        int i = 0;
        int mul = 0;
        int sum = 0;
        for(i = 1;i <= 5;i++) {
            int j = 0;
            mul = 1;
            for(j = 1;j <= i;j++) {
                mul *= j;
            }
            sum += mul;
        }
        return sum;
    }
    public static void main3(String[] args) {
        //写一个方法，求阶乘相加，如：1！+2!....
        System.out.println(func());
    }

//    public static int add(int a) {
//        return a*2;
//    }
//    public static int add(int a,int b) {
//        return a + b;
//    }
//    public static double add(double a,double b) {
//        return a + b;
//    }
//    public static int add(int a,int b,short c) {
//        return a + b + c;
//    }
    public static void main4(String[] args) {
        //函数重载：
        //什么样的函数能被称为重载函数呢？
        //  1.方法名相同
        //  2.形式参数列表不同，包括：（个数，类型，顺序），任意一个不同即可
//        System.out.println(add(4));//8
//        System.out.println(add(2,4));//6
//        System.out.println(add(10.3,5.4));//15.7000
//        System.out.println(add(1,2,(short)3));//6
    }
    //递归
    //解决递归问题：
    //   1.有一个递推公式（这个是递归难的地方）
    //   2.找到当前问题递归的“结束条件”（等同于起始条件）
    public static void func1(int input) {
//        if(input / 10 == 0) {
//            System.out.print(input+" ");
//            return;
//        }else {
//            func1(input / 10);
//            System.out.print((input % 10)+" ");
//        }
        if(input / 10 != 0) {
            func1(input/10);
        }else {
            System.out.print(input+" ");
            return;
        }
        System.out.print((input % 10)+" ");
    }
    public static void main5(String[] args) {
        //代码：顺序打印一个整数的每一位。 如：1234 --> 1 2 3 4
        Scanner s = new Scanner(System.in);
        int input = s.nextInt();
        func1(input);
    }
    public static int func2(int input) {
        if(input == 0)  {
            return 0;
        }else {
            return input + func2(input - 1);
        }
    }

    public static void main6(String[] args) {
        //使用递归求：1+2+...+10
        int n = func2(10);
        System.out.println(n);
    }
    public static int func3(int n) {
        int sum = 0;
        if(n <= 9) {
            return n;
        }else {
            sum += n % 10 + func3(n / 10);
        }
        return sum;
    }
    public static void main7(String[] args) {
        //写一个方法：输入一个非负整数，返回组成他的数字之和
        //如：1729  返回 1+7+2+9 = 19
        Scanner s = new Scanner(System.in);
        int input = s.nextInt();
        int ret = func3(input);
        System.out.println(ret);
    }

    public static void main(String[] args) {
        //使用循环，求斐波那契数列的第n项
        Scanner s = new Scanner(System.in);
        int n = s.nextInt();
        int f1 = 1;
        int f2 = 1;
        int f3 = 0;
        for(int i = 3;i <= n;i++) {
            f3 = f1 + f2;
            f1 = f2;
            f2 = f3;
        }
        System.out.println(f3);
    }
}
