package demo1;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-10-22
 * Time: 19:39
 */
public class 杨辉三角 {
    public List<List<Integer>> generate (int numRows) {
        List<List<Integer>> ret = new ArrayList<>();
        //设立第一行，1，有了这个 1，才能进行下去；
        List<Integer> list0 = new ArrayList<>();
        list0.add(1);
        ret.add(list0); // 将 list0 这个顺序表 放进 ret 这个二维顺序表里。

        // 从第二行开始操作，实现杨辉三角，使用for循环，设立 int i = 1，开始一行一行的操作
        for (int i = 1; i < numRows ; i++) {
            // 建立每一行的一个数组（顺序表）
            List<Integer> cruRow = new ArrayList<>();

            // 处理数组的第一项，设置为 1
            cruRow.add(1);

            // 处理中间行
            // 获取上一行(i-1)的整行数据 根据我们的算法：[i][j] = [i-1][j]+[i-1][j-1]
            List<Integer> preRow = ret.get(i-1);

            for (int j = 1; j < i; j++) {
                int val1 = preRow.get(j); // 相当于 int val1 = ret[i-1][j]
                int val2 = preRow.get(j-1); // 相当于 int val1 = ret[i-1][j-1]
                cruRow.add(val1+val2);
            }


            // 处理尾巴,同样的，设置为 1
            cruRow.add(1);

            //最后把处理好的这一行数据（数组），放进 ret 这个 二维顺序表里面
            ret.add(cruRow);
        }
        return ret;
    }
    public static void main(String[] args) {
        杨辉三角 y = new 杨辉三角();
        int input = 4;
        List<List<Integer>> ret = y.generate(input);
        for (int i = 0;i<input;i++) {
            System.out.println(ret.get(i));
        }
    }
}
