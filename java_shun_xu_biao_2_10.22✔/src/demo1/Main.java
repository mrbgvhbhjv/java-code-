package demo1;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-10-22
 * Time: 18:56
 */
public class Main {
    public static void main2(String[] args) {
        // add 方法的简单介绍：
        ArrayList<Integer> list = new ArrayList<>();
        list.add(1); // 直接在数组最后面添加
        list.add(1,99); // 向指定下标位置添加元素
        System.out.println(list);

        // addAll 尾插 一个顺序表,如：向新的顺序表里 插入 list
        ArrayList<Integer> test = new ArrayList<>();
        test.add(10);
        test.add(1,20);
        test.addAll(list);
        System.out.println(test);

        //remove 删除数组元素 由两种删除的方式：
        // 1. 传递数组下标，去删除该下标的元素：
        list.remove(1);
        System.out.println(list);
        System.out.println(test);
        // 这里的删除，并不影响已经使用未删除的 list 去 拼接的 test顺序表。
        // 如果 我想指定删除某一个明确的对象，比如我想删除 99 ，该怎么删呢？
        // test.remove(99); 错误写法。

        // 传递的是一个明确的对象，删除这个明确的对象
        test.remove(new Integer(99));
        System.out.println(list);
        System.out.println(test);

    }
    public static void main1(String[] args) {
        // 两种不同的定义 顺序表 的方式：
        //1. 通过 ArrayList 类型进行 创建
        ArrayList<Integer> list = new ArrayList<>();

        //2. 通过 List 接口 来进行 创建
        List<Integer> test = new ArrayList<>();

        // 两种创建方式的区别：
        // 第一种，可以调用 ArrayList 类 内所有的 方法
        // 第二种，通过 List接口，只能调用 List接口里面的方法，有可能 ArrayList类内有某个方法，而 List接口里面没有这个方法
    }
}
