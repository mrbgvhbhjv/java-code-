package demo1;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-10-09
 * Time: 21:05
 */
public class IllegalException extends RuntimeException{
    public IllegalException() {
        super();
    }
    public IllegalException(String message) {
        super(message);
    }
}
