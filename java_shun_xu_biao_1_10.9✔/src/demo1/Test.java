package demo1;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-10-09
 * Time: 20:35
 */
public class Test {
    public static void main(String[] args) {
        MyArrayList myArrayList = new MyArrayList();
        myArrayList.add(1);
        myArrayList.add(2);
        myArrayList.add(3);
        myArrayList.add(4);
        myArrayList.add(5);

        myArrayList.set(1,999);
        myArrayList.display();

        System.out.println();

        myArrayList.remove(999);
        myArrayList.display();

        System.out.println();

        myArrayList.clear();
        myArrayList.display();
        System.out.println(myArrayList.size());
    }
    public static void main5(String[] args) {
        MyArrayList myArrayList = new MyArrayList();
        myArrayList.add(10);
        myArrayList.add(1,20);
        myArrayList.display();

        System.out.println();

        System.out.println(myArrayList.get(1));

        myArrayList.set(1,999);
        System.out.println(myArrayList.get(1));


    }
    public static void main4(String[] args) {
        MyArrayList myArrayList = new MyArrayList();
        myArrayList.add(2);
        System.out.println(myArrayList.contains(2));
        System.out.println(myArrayList.indexOf(2));
    }
    public static void main3(String[] args) {
        MyArrayList myArrayList = new MyArrayList();
        myArrayList.add(1);
        myArrayList.add(2);
        myArrayList.add(3);
        myArrayList.add(4);
        myArrayList.add(5);

        myArrayList.add(1,10);
        myArrayList.add(6,23);
        myArrayList.add(7,43);
        myArrayList.add(8,50);
        myArrayList.add(9,210);
        myArrayList.add(10,2323);
        myArrayList.display();
    }
    public static void main2(String[] args) {
        MyArrayList myArrayList = new MyArrayList();
        myArrayList.add(1);
        myArrayList.add(2);
        myArrayList.add(3);
        myArrayList.add(4);
        myArrayList.add(5);

        myArrayList.display();
    }
    public static void main1(String[] args) {
        MyArrayList myArrayList = new MyArrayList();
        MyArrayList myArrayList1 = new MyArrayList();
        System.out.println(myArrayList.size());
        System.out.println(myArrayList1.size());
    }
}
