package demo1;

import java.util.Arrays;
public class MyArrayList implements IList{

    private int[] array;
    private int usedSize;
    public static final int DEFAULT_CAPACITY = 10;

    // 默认的构造方法
    public MyArrayList() {
        array = new int[DEFAULT_CAPACITY];
    }

    // 新增元素,默认在数组最后新增
    public void add(int data) {
        if(isFull()) {
            grow();
        }
        array[usedSize] = data;
        usedSize++;
    }
    //判断数组是否 满了
    private boolean isFull() {
        return usedSize == array.length;
    }
    // 扩容数组大小
    private void grow() {
        this.array = Arrays.copyOf(this.array,2*this.array.length);
    }

    // 在 pos 位置新增元素
    public void add(int pos, int data) {
        // 判断 pos（下标）的合理性
        try{
            checkpos1(pos);
            if(isFull()) {
                grow();
            }
            // 挪动选择插入的对应下标的元素及其他后面的元素，统统往后移。
            for (int i = usedSize-1;i >= pos;i--) {
                array[i+1] = array[i];
            }

            // 放置元素
            array[pos] = data;

            // 动态 计数 + 1
            usedSize++;
        }catch (IllegalException e) {
            e.printStackTrace();
        }
    }

    // 判断下标是否合理
    private void checkpos1(int pos) throws IllegalException{
        if(pos < 0 || pos > this.usedSize) {
            throw new IllegalException("pos位置不合理");
        }
    }

    // 判定是否包含某个元素
    public String contains(int toFind) {
        for (int i = 0; i < this.usedSize; i++) {
            if(this.array[i] == toFind) {
                return "存在你要找的元素";
            }
        }
        return "不存在你要找的元素";
    }
    // 查找某个元素对应的位置
    public int indexOf(int toFind) {
        for (int i = 0; i < this.usedSize; i++) {
            if(this.array[i] == toFind) {
                return i;
            }
        }
        return -1;
    }
    // 获取 pos 位置的元素
    public int get(int pos) {
        try {
            checkEmpty();
            checkpos2(pos);
            return this.array[pos];
        }catch (EmptyException e) {
            e.printStackTrace();
        }catch (IllegalException e) {
            e.printStackTrace();
        }
        return -1;
    }
    //判断 数组是否为空
    private void checkEmpty() throws EmptyException{
        if(usedSize == 0) {
            throw new EmptyException("该数组为空");
        }
    }
    // 判断下标是否合理
    private void checkpos2(int pos) throws IllegalException{
        if(pos < 0 || pos >= this.usedSize) {
            throw new IllegalException("pos位置不合理");
        }
    }
    // 给 pos 位置的元素设为 value，前提是：该位置要有元素。
    public void set(int pos, int value) {
        try {
            checkpos2(pos);
            this.array[pos] = value;
        }catch (IllegalException e) {
            e.printStackTrace();
        }
    }
    //删除第一次出现的关键字key
    public void remove(int toRemove) {
        // 找到需要删除的元素的下标
        int pos = indexOf(toRemove);

        // 循环覆盖
        for (int i = pos;i < usedSize-1;i++) {
            this.array[i] = this.array[i+1];
        }

        // 自动 计数-1
        usedSize--;
    }
    // 获取顺序表长度
    public int size() {
        return this.usedSize;
    }
    // 清空顺序表
    public void clear() {
        // 如果是 类 ，就得 将数组里面的元素，全都置为 null，防止内存泄漏
//        for (int i = 0;i < usedSize;i++) {
//            this.array[i] = null;
//        }
        usedSize = 0;
    }

    // 打印顺序表，注意：该方法并不是顺序表中的方法，为了方便看测试结果给出的
    public void display() {
        for (int i = 0; i < this.usedSize; i++) {
            System.out.print(array[i]+ " ");
        }
    }
}
