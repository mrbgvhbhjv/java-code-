package demo1;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-10-09
 * Time: 21:33
 */
public class EmptyException extends RuntimeException{
    public EmptyException() {
        super();
    }
    public EmptyException(String msg) {
        super(msg);
    }
}
