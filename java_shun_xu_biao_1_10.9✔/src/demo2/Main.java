package demo2;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-10-21
 * Time: 13:18
 */
public class Main {
    public static void main(String[] args) {
        MyArrayList myArrayList = new MyArrayList();

        myArrayList.add(11);
        myArrayList.add(12);
        myArrayList.add(13);
        myArrayList.add(14);
        myArrayList.add(15);

        myArrayList.display();

        myArrayList.set(0,999);
        myArrayList.display();


        myArrayList.remove(15);
        myArrayList.display();

        myArrayList.clear();
        myArrayList.display();
    }
    public static void main4(String[] args) {
        MyArrayList myArrayList = new MyArrayList(5);
        myArrayList.add(1);
        myArrayList.add(2);
        myArrayList.add(3);
        myArrayList.add(4);
        myArrayList.add(5);
        myArrayList.add(1,99);
        myArrayList.add(6,10);

        System.out.println(myArrayList.contains(100));

        System.out.println(myArrayList.indexOf(99));

        System.out.println(myArrayList.get(1));


    }
    public static void main3(String[] args) {
        MyArrayList myArrayList = new MyArrayList(5);
        myArrayList.add(1);
        myArrayList.add(2);
        myArrayList.add(3);
        myArrayList.add(4);
        myArrayList.add(5);
        myArrayList.add(1,99);
        myArrayList.add(6,10);
        System.out.println(myArrayList.size());
        myArrayList.display();
    }
    public static void main2(String[] args) {
        MyArrayList myArrayList = new MyArrayList();
        myArrayList.add(1);
        myArrayList.add(2);
        myArrayList.add(2);
        myArrayList.add(2);
        myArrayList.add(2);
        myArrayList.add(2);
        myArrayList.add(2);
        myArrayList.add(2);
        myArrayList.add(2);
        myArrayList.add(2);
        myArrayList.add(2);
        myArrayList.add(2);
        myArrayList.add(2);
        System.out.println(myArrayList.size());
        myArrayList.display();
    }
    public static void main1(String[] args) {
        MyArrayList myArrayList = new MyArrayList();
        System.out.println(myArrayList.size());
    }
}
