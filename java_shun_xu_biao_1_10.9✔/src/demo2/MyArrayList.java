package demo2;

import demo1.EmptyException;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-10-21
 * Time: 13:20
 */
public class MyArrayList implements IList{
    private int[] array;
    private int usedSize;
    private int DEFAULT_CAPACITY = 10;

    public MyArrayList() {
        this.array = new int[DEFAULT_CAPACITY];
    }

    public MyArrayList(int num) {
        this.array = new int[num];
    }

    @Override
    public void add(int data) {
        // 判断数组是否已满
        if(isFull()) {
            grow();
        }
        array[this.usedSize] = data;
        this.usedSize++;
    }

    private boolean isFull() {
        return usedSize == array.length;
    }

    private void grow() {
        this.array = Arrays.copyOf(this.array,this.array.length * 2);
    }

    private void checkPos(int pos) {
        if(pos < 0 || pos > this.usedSize) {
            throw new PosIllegalException("下标位置不合理");
        }
    }

    @Override
    public void add(int pos, int data) throws PosIllegalException{
        try{
            // 判断下标是否合理
            checkPos(pos);
            // 判断数组是否已满
            if(isFull()) {
                grow();
            }
            // 往后移动数组元素，腾出位置，给予插入的位置
            for (int i = this.usedSize -1; i >= pos ; i--) {
                this.array[i+1] = this.array[i];
            }
            //放置元素
            this.array[pos] = data;

            this.usedSize++; // 标记数加 1
        }catch (PosIllegalException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String contains(int toFind) {
        for (int i = 0; i < this.usedSize; i++) {
            if(this.array[i] == toFind) {
                return "存在你要找的元素";
            }
        }
        return "不存在你要找的元素";
    }

    @Override
    public int indexOf(int toFind) {
        for (int i = 0; i < this.usedSize; i++) {
            if(this.array[i] == toFind) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public int get(int pos) {
        try{
            // 先判断数组是不是空数组
            checkEmpty();
            // 然后下标是否合理
            checkPos2(pos);

            return this.array[pos];
        }catch(PosIllegalException e) {
            e.printStackTrace();
        }catch(EmptyException e) {
            e.printStackTrace();
        }
        return -1;
    }

    private void checkPos2(int pos) {
        if(pos < 0 || pos >= this.usedSize) {
            throw new PosIllegalException("位置下标不合理");
        }
    }

    private void checkEmpty() {
        if(this.usedSize == 0) {
            throw new EmptyException("该数组为空，无法获取元素");
        }
    }


    @Override
    public void set(int pos, int value) {
        try{
            checkPos2(pos);
            this.array[pos] = value;
        }catch (PosIllegalException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void remove(int toRemove) {
        // 找到需要删除的元素的下标
        int pos = indexOf(toRemove);

        // 循环覆盖
        for (int i = pos; i < this.usedSize - 1;i++) {
            this.array[i] = this.array[i+1];
        }

        // 标记数 自动 减一
        this.usedSize--;
    }

    @Override
    public int size() {
        return this.usedSize;
    }

    @Override
    public void clear() {
        this.usedSize = 0;
    }

    @Override
    public void display() {
        for (int i = 0; i < this.usedSize; i++) {
            if(i == 0) {
                System.out.print("["+ this.array[i]);
            } else if (i == this.usedSize-1) {
                System.out.print(","+this.array[i]+"]");
            }else {
                System.out.print(","+this.array[i]);
            }
        }
        System.out.println();
    }
}
