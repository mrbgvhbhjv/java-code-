package demo2;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-10-22
 * Time: 15:15
 */
public class PosIllegalException extends RuntimeException{
    public PosIllegalException() {
        super();
    }
    public PosIllegalException(String msg) {
        super(msg);
    }
}
