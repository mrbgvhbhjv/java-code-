package demo2;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-10-22
 * Time: 15:39
 */
public class EmptyException extends RuntimeException{
    public EmptyException() {
        super();
    }
    public EmptyException(String msg) {
        super(msg);
    }
}
