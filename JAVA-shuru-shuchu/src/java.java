import java.util.Random;
import java.util.Scanner;
import java.util.stream.StreamSupport;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-05-08
 * Time: 22:15
 */
public class java {
    //输入一个字符串，并输出
    //并比较 next() 和 next.Line() 的区别
    public static void main1(String[] args) {
        Scanner str = new Scanner(System.in);
        String str1 = str.nextLine();// next.Line() 的使用
        System.out.println(str1);
        String str2 = str.next();// next() 的使用
        System.out.println(str2);
        //next() 和 next.Line() 的区别是：
    //next()  当你输入的字符串带有 空格 时，读取到的是 空格之前 的字符串
    //next.Line()   当你输入的字符串带有 空格 时，会把 包括空格 的你输入的 整个字符串 一起读取
    }

    public static void main2(String[] args) {
        //读入一个整数，输出你输入的整数
        Scanner s = new Scanner(System.in);
        System.out.print("请输入一个整数：");
        int input = s.nextInt();
        System.out.println("您输入的整数是"+input);

        s.nextLine();
        //读入一个字符串，并输出
        System.out.print("请输入一个字符串：");
        String str = s.nextLine();
        System.out.println(str);
        //注意： 输入字符串的操作放在这里会有问题，当你输入完整数后，你按下 ”回车“
        // 回车 会被这里的操作识别，读入一个 回车 所以，就无法输入字符串了
        //解决方法： 1.把这步操作放在第一步。
        //         2.在字符串前面加上  （Scanner后面定义的名字）.nextLine  去吸收 回车

        //读入一个小数，输出你输入的小数
        System.out.println();
        System.out.println("输入一个小数：");
        double input1 = s.nextDouble();
        System.out.println(input1);
    }

    public static void main3(String[] args) {
        //写一个猜数字游戏（思维和 C语言 的是一样的）
        Random ran = new Random();
        int randNum = ran.nextInt(100);
        System.out.println(randNum);
        Scanner san = new Scanner(System.in);
        while(true) {
            System.out.print("请输入你猜的数字：");
            int input = san.nextInt();
            if(input < randNum) {
                System.out.println("猜小了");
            } else if(input > randNum) {
                System.out.println("猜大了");
            }else {
                System.out.println("猜对了");
                break;
            }
        }
    }

    public static void main(String[] args) {

    }
}
