package demo1;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-05-24
 * Time: 21:10
 */
class test {
    public int a;
    public int b;
    public String name = "小华";
    //当出现多个实例化代码块的时候，谁先定义，谁先执行
    //同时：也是需要 对象的实例化（创建一个对象），才能执行。
    {
        a = 1;
        b = 2;
        System.out.println("实例代码块1");
    }
    {
        a = 1;
        b = 3;
        System.out.println("实例代码块2");
    }
    //当出现多个静态代码块的时候，谁先定义，谁先执行
    //同时：也是需要 对象的实例化（创建一个对象），才能执行。
    //静态代码块的执行顺序是 第一个
    static {
        System.out.println("静态代码块1");
    }
    static {
        System.out.println("静态代码块2");
    }
//    public test(){
//        System.out.println("执行构造方法");
//    }

    //alt insert ---> toString() ---> 选择 非静态 成员变量
    @Override
    public String toString() {
        return "test{" +
                "a=" + a +
                ", b=" + b +
                ", name='" + name + '\'' +
                '}';
    }
}
public class Test2 {
    public static void main(String[] args) {
        test a = new test();
        System.out.println("=================");
        test b = new test();
        //由运行结果可知：
        // 1. 静态代码块，只在类加载的时候执行，只执行一次。后面再创建一个新对象，不会再执行
        // 2.静态代码块，实例（构造）代码块，构造方法，三者的执行顺序为：
        //    静态代码块 --> 实例（构造）代码块 --->  构造方法

        //对象的打印
        System.out.println(a);//  sout(对象的引用名) ， 就可以实现对 非静态 成员变量的打印。

    }
}
