package demo1;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-05-24
 * Time: 19:17
 */
//静态成员方法的初始化：
    //1.构造方法
    //2.就地初始化
    //3.代码块
    //4.通过 get 和 set 方法 ，初始化
class Dog{
    public int age = 10;  //就地初始化
    public String name;
    public static String classname;
//    public Dog(String name,int age){ //构造方法
//        this.name = name;
//        this.age = age;
//    }
{   //实例（构造）代码块
    name = "小黄";
    age = 10;
}
    public void print() {  //非静态成员方法
        System.out.println(" 姓名："+this.name+" 年龄："+this.age);
    }
    static {  //静态代码块 ---> 一般用来初始化， 静态成员变量 。
        //当类被加载的时候，静态代码块就会执行。
        classname="1班";
        System.out.println("调用了静态代码块");
    }
}
public class Test1 {
    public static void main(String[] args) {
        Dog dog1  = new Dog();
        //new 一个对象的时候，就会执行 实例（构造）代码块
        System.out.println(dog1.name);
        System.out.println(dog1.age);
    }
    public static void main2(String[] args) {
        //代码块实例化
        Dog student = new Dog();
    }
    public static void main1(String[] args) {
        // 构造方法初始化
        Dog dog = new Dog(); //实例化一个对象
        dog.print(); //访问非静态成员方法
    }
}
