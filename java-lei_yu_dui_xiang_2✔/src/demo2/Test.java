package demo2;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-09-17
 * Time: 22:05
 */
class Dog {
    public static int age;// 就地初始化

    static { // 静态代码块
        age = 10;
    }

    public static int getAge() {
        return age;
    }

    public static void setAge(int age) {
        Dog.age = age;
    }

    public Dog(int age) {
        this.age = age;
        // 因为构造方法，是用过 this.成员变量 来初始化的，
        // 所以，不建议使用构造方法来对静态成员变量进行初始化
    }

    public void func1(Dog this) { // 非静态方法
        System.out.println("非静态方法");
        test();
    }

    public static void func2() { // 静态方法
        System.out.println("静态方法");
        test();
    }

    public static void test() { //静态方法
        System.out.println("测试方法");
    }
}
public class Test {
    public static void main(String[] args) {
        Dog dog = new Dog(10); // 实例化对象

    }
}

//        //通过 对象的引用 进行访问
//        dog.test();
//
//        //通过 类名 进行访问
//        Dog.test();
