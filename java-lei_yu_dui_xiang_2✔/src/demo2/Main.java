package demo2;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-09-17
 * Time: 20:45
 */
class Student {
    private String name;
    private int age;
    private String stuNum;
    public static String className;

    public Student(String name, int age, String stuNum) {
        this.name = name;
        this.age = age;
        this.stuNum = stuNum;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getStuNum() {
        return stuNum;
    }

    public void setStuNum(String stuNum) {
        this.stuNum = stuNum;
    }
}
public class Main {
    public static void main(String[] args) {
        //static 修饰成员变量---> 静态成员变量
        Student student = new Student("zhangsan",10,"1234");

        //通过 当前对象的引用 进行访问
        student.className = "1班";//编译器不会报错，但是，不推荐这种访问方式，

        //通过 类名 进行访问
        Student.className = "2班";

        student.setName("小黄");
        student.setAge(10);
        System.out.println(" 姓名："+ student.getName()+"  年龄： "+
                student.getAge()+"  班级："+ Student.className);
        //注意：静态的---> 不依赖与对象   意味着，不需要当前对象的引用进行访问
    }
}
//静态成员变量的访问方式：
//  1.可以通过 当前对象的引用 进行访问，如 student.classname;（可行，但是，不推荐！！！！！）
//  2.通过 类名 进行访问，如：Student.classname;（更推荐这种，因为静态成员变量，属于类变量，不是对象）