/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-05-20
 * Time: 19:18
 */

//    class student{
//        public String name;
//        public int age;
//        public String classnum;
//
//    public student(String name, int age, String classnum) {
//        this.name = name;
//        this.age = age;
//        this.classnum = classnum;
//    }  //快速创建构造方法，右键(alt+Insret)-->构造方法-->按住Ctrl选择属性
//
//    public student() { //无参数的构造函数
//            this("小红",19,"12345");//this调用当前对象的其他构造方法，必须写在第一行
//        }
//
//        public void show(){
//            System.out.println(" 姓名："+this.name+"  年龄： "+this.age+"  学号： "+this.classnum);
//        }
//}


//    class Student{  //现在将这三个成员变量，使用修饰限定访问符private，使得这三个变量只能在 类内 进行使用
//        private String name;
//        private int age;
//        private String classnum;
//
//    public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    public int getAge() {
//        return age;
//    }
//
//    public void setAge(int age) {
//        this.age = age;
//    }
//
//    public String getClassnum() {
//        return classnum;
//    }
//
//    public void setClassnum(String classnum) {
//        this.classnum = classnum;
//    }
//}

class Student{  //现在将这三个成员变量，使用修饰限定访问符private，使得这三个变量只能在 类内 进行使用
        private String name;
        private int age;
        private String classnum;
        public static String classname;//定义了一个静态的公开成员变量 classname
        public static int count;

    public String getName() {
        return name;
    }

    public  void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getClassnum() {
        return classnum;
    }

    public void setClassnum(String classnum) {
        this.classnum = classnum;
    }
}
public class java {
//    public static void main(String[] args) {
//        //定义一个类，描述学生的，创建对象，访问成员变量，成员函数，打印学生信息
//        student s = new student("小明",10,"23407");
//        s.show();
//
//        //创建第二个对象，打印对象信息。
//        student s1 = new student("小红",19,"12345");
//        //s1.age = 10;//执行完 构造函数 后，改变age的值
//        s1.show();
//
//        //创建第三个对象，由于对象都是public，所以，在类外进行访问，进行初始化
//        student s2 = new student();
//        s2.show();//由于在无参的构造方法里面，调用了带有三个参数的构造方法，
//        // 且传的三个参数与s1指向的对象一样，所以输入结果与s1指向的对象一样。
//    }

    public static void main1(String[] args) {
        //简单使用封装

        //封装：对类进行封装，把内部细节隐藏起来，必须要在类里面提供公开的（pubilc）的方法，进入类，
        // 就可以访问被封装（private）起来的成员变量了
        //在类当中，有setXxx 和 getXxx 可以对成员变量进行赋值，和获取成员变量的值。
        Student student = new Student();
        student.setName("小黄");
        student.setAge(10);
        student.setClassnum("123");
        System.out.println(" 姓名："+ student.getName()+"  年龄： "+
                student.getAge()+"  学号： "+ student.getClassnum());
    }

    public static void main2(String[] args) {
        //static 修饰成员变量---> 静态成员变量
        //被static修饰的成员变量，不是对象，是类变量
        Student student = new Student();
        //静态成员变量的访问方式：
        //  1.可以通过 当前对象的引用 进行访问，如 student.classname;（可行，但是，不推荐！！！！！）
        //  2.通过 类名 进行访问，如：Student.classname;（更推荐这种，因为静态成员变量，属于类变量，不是对象）
        student.classname = "1班";//编译器不会报错，但是，不推荐这种访问方式，
        // 因为静态的成员变量（成员方法），不依赖于对象

        Student.classname = "2班";//对 静态成员变量 进行初始化。
        student.setName("小黄");
        student.setAge(10);
        student.setClassnum("123");
        System.out.println(" 姓名："+ student.getName()+"  年龄： "+
                student.getAge()+"  学号： "+ student.getClassnum()+"  班级："+Student.classname);
        //注意：静态的---> 不依赖与对象   意味着，不需要当前对象的引用进行访问
    }

    public static void main3(String[] args) {
        //依赖对象的，会造成空指针异常
//        Student student  = null;//现在 这个引用的指向是随机的，没有实例化对象
//        System.out.println(student.name);//出现空指针异常

        Student student1 = null;
        student1.classname = "1班";
        System.out.println(student1.classname);//正常打印 1班
        //说明了 静态的成员变量（成员方法），不依赖于对象，所以不会出现空指针异常
    }

    public static void main4(String[] args) {
        //体验一下，静态成员变量的访问的两种方式。
        Student student1 = new Student();
        student1.count++;

        Student student2 = new Student();
        student2.count++;

        Student student3 = new Student();
        student3.count++;

        Student.count--;

        System.out.println(Student.count);//结果是 2
        //因为 无论是，通过当前对象的引用去访问 静态成员变量，还是通过 类名 去访问静态成员变量，都可以访问到
        //但是，静态成员变量 是属于 类变量，不属于对象的， 使用 类名 去访问 更加合理。
    }

    public static void main5(String[] args) {

    }
}
