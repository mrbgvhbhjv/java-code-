package Test;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-05-21
 * Time: 10:55
 */
class Student {
    private String name;
    private int age;
    private double score;
    public static String classname;
    public Student(){  //不带参数的构造方法
        this("小明",20,90.0);//this 调用，必须是 第一条语句
//        System.out.println("利用this关键字，访问带有三个参数的构造方法");
        //在无参数的构造方法里面，利用 this关键字，调用其他的构造方法
    }

    public Student(String name, int age, double score) {
        this.name = name;
        this.age = age;
        this.score = score;
//        System.out.println("调用了带有三个参数的构造方法");
    }
    public static void fun() {
        //this.name;//这里会报错，因为，在静态成员方法里面，不可以直接使用任何 非静态的成员变量或者成员方法
        ;
    }
    public void show() {
        System.out.println(" 姓名："+this.name+" 年龄："+this.age+" 分数："+this.score+" 班级："+Student.classname);
    }

    public static void setClassname(String classname) { //static 修饰的成员方法。
        Student.classname = classname;
    }
}
public class stu {
    public static void main1(String[] args) {
        Student student = new Student();//这里会直接调用不带参数的构造方法，
        // 在不带参数构造方法里面，又利用this关键字，访问带有三个参数的构造方法，进而进行赋值了
        Student.setClassname(" 1班");// 使用类名去访问静态成员方法。
        student.show();
    }
}
