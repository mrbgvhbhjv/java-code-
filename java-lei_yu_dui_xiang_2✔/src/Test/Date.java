package Test;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-05-21
 * Time: 10:43
 */
                        //  static 修饰成员方法
    //和静态成员方法一样，   静态成员方法 ---> 类方法 ---- > 不依赖于对象 ---> 直接通过类名进行访问

    //静态成员方法的访问方式：
    //   1.通过对象的引用 进行访问（可以，但是因为类方法，是不依赖于对象的，所以，很不推荐！！！！！）
    //   2.通过 类名 进行访问（最好用这种方式去访问，养成习惯）
class Test {
    public int age;
    public String name;
    public void sleep() {
        test();//非静态成员方法里面，可以调用静态成员方法
        System.out.println(this.name+"在睡觉");
    }
    public static void eat(){
//        System.out.println(this.name+"在吃饭"); // 在静态成员方法里面，不可以直接调用任何 非静态成员变量。
//        sleep();                                // 在静态成员方法里面，不可以直接调用任何 非静态成员方法。
       Test s = new Test();            //在静态成员方法 里面，创建新的对象，通过对象，可以访问 非静态成员变量。
        System.out.println(s.name+"在吃饭");
    }
    public static void test() {
        System.out.println("测试在非静态成员方法里面，调用静态成员方法");
    }
}

public class Date {
    public static void main(String[] args) {
        Test stu = new Test();
        stu.sleep();
        Test.eat();// 通过类名访问 静态成员变量

        //调用静态成员方法的时候，是没有 隐参传引用给 this的。所以，在静态成员方法里面，无法使用 this。
        }
    }