package demo4;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-08-30
 * Time: 17:17
 */
public class School {
    //组合的写法（语法）
    public Teacher[] teachers = new Teacher[10];
    public Student[] students = new Student[10];
}
