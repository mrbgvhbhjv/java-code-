package demo1;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-08-28
 * Time: 20:39
 */
public class Animal {
    public String name;
    public int age;
    public double weight;
    public void eat() {
        System.out.println(this.name+"在吃饭...");
    }
}
