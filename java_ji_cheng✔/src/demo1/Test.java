package demo1;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-08-28
 * Time: 20:39
 */
public class Test {
    public static void main(String[] args) {
        Dog dog = new Dog();
        dog.name = "小黄";
        dog.bark();
        System.out.println("===============");

        Cat cat = new Cat();
        cat.name = "小虎";
        cat.mimi();
    }
}
