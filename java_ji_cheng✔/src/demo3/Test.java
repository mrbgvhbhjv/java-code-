package demo3;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-08-30
 * Time: 10:20
 */
public class Test {
    public static void main(String[] args) {
        Dog dog = new Dog("旺财",10,30.8);
//        dog.name = "旺财";
        dog.eat();
        dog.bark();

        Cat cat = new Cat("小虎",6,20.6);
//        cat.name = "咪咪";
        cat.eat();
        cat.mimi();
    }
}
