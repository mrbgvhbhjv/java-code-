package demo3;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-08-30
 * Time: 10:19
 */
public class Dog extends Animal{
    public Dog(String name, int age, double weight) {
        super(name, age, weight);
    }
//    public Dog() {
//        super("旺财",10,30.9);
//    }

    public void bark() {
        System.out.println(this.name+" 在汪汪叫.....");
    }
}
