package demo3;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-08-30
 * Time: 10:19
 */
public class Cat extends Animal{
    public Cat(String name, int age, double weight) {
        super(name, age, weight);
    }
//    public Cat() {
//        super("小虎",12,23.4);
//    }

    public void mimi() {
        System.out.println(this.name+" 在咪咪叫");
    }
}
