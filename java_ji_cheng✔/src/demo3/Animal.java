package demo3;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-08-30
 * Time: 10:19
 */
public class Animal {
    public String name;
    public int age;
    public double weight;

    public Animal(String name, int age, double weight) {
        this.name = name;
        this.age = age;
        this.weight = weight;
    }

    public void eat() {
        System.out.println(this.name+" 在吃饭....");
    }
}
