package demo2;

import demo4.Student;
import demo4.TestBase;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-08-30
 * Time: 17:18
 */
public class TestProtect extends TestBase {
    public static void main(String[] args) {
        TestProtect testProtect = new TestProtect();
        //验证 protected 的访问权限。
        testProtect.a = 10;
        testProtect.b = 10;
        System.out.println(testProtect.a + testProtect.b);
    }

}
