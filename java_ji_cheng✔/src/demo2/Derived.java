package demo2;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-08-29
 * Time: 20:52
 */
public class Derived {
    public int a = 1;
    public int b = 2;
    public void test1() {
        System.out.println("父类的test1()....");
    }
    public void test2() {
        System.out.println("Derived(父类)....");
    }
}
