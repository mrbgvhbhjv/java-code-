package demo2;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-08-29
 * Time: 20:52
 */
public class Base extends Derived{
    public int c = 3;
    public int b = 200;
    public int a = 100;//子类与父类有相同的变量的时候，优先使用子类的
    public void test1(char a) {
        System.out.println("子类的test1(char a)");
    }
    public void test2() {
        System.out.println("Base(子类)....");
    }
    public void func2() {
        //当父类和子类有相同的方法的时候，优先访问子类的方法
        //同时，这样的情况属于 方法的重写。

        this.test2();//调用子类的方法

        //如果想要访问父类的方法，使用 super关键字 或者 new父类的对象，通过对象的引用来访问。

        //使用super关键字
        super.test2();
        //new 对象
        Derived derived = new Derived();
        derived.test2();

    }
    public void func1() {
        this.test1();
        this.test1('a');
        //test1 构成了方法的重载。
    }
    public void func() {
//        System.out.println("a="+this.a);
//        System.out.println("父类的a="+super.a);
        //通过 super 关键字 可以访问 父类的成员变量
//        System.out.println("b="+this.b);
//        System.out.println("父类的b="+super.b);
//        System.out.println("c="+this.c);

        //当然也可以通过new对象 ，来访问父类的成员
        Derived derived = new Derived();
        System.out.println(derived.a);
        System.out.println(derived.b);
    }
}
