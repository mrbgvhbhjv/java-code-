import User.AdminUser;
import User.NormalUser;
import User.User;
import book.BookList;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-09-10
 * Time: 16:16
 */
public class Main {
    public static User login() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入你的姓名：");
        String name = scanner.nextLine();

        while(true) {
            System.out.println("请选择你的身份：1.管理员 , 2.普通用户：");
            int choice = scanner.nextInt();

            if(choice == 1) {
                return new AdminUser(name);
            } else if (choice == 2) {
                return new NormalUser(name);
            }else {
                System.out.println("输入错误，请重新选择：");
                continue;
            }
        }
    }
    public static void main(String[] args) {
        BookList bookList = new BookList();
        User user = login();
        while(true) {
            int choice = user.menu();
            user.doOperation(choice,bookList);
        }

    }
}
