package book;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-09-10
 * Time: 15:51
 */
public class BookList {
    private Book[] Books = new Book[10];
    private int UsedSize;

    public BookList() {
        Books[0] = new Book("西游记","吴承恩","小说",10);
        Books[1] = new Book("红楼梦","曹雪芹","小说",15);
        Books[2] = new Book("三国演义","罗贯中","小说",20);
        this.UsedSize = 3;
    }

    public int getUsedSize() {
        return UsedSize;
    }

    public void setUsedSize(int usedSize) {
        UsedSize = usedSize;
    }

    public Book getBook(int pos) {
        return Books[pos];
    }

    public void setBook(int pos,Book book) {
        Books[pos] = book;
    }
}
