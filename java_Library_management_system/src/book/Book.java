package book;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-09-10
 * Time: 15:50
 */
public class Book {
    private String BookName;
    private String author;
    private String type;
    private int price;
    private boolean isBorrowed;

    public Book(String bookName, String author, String type, int price) {
        BookName = bookName;
        this.author = author;
        this.type = type;
        this.price = price;
    }

    @Override
    public String toString() {
        return "book{" +
                "BookName='" + BookName + '\'' +
                ", author='" + author + '\'' +
                ", type='" + type + '\'' +
                ", price=" + price +","+
                ((isBorrowed == true) ? "已借出":"未借出")+
//                ", isBorrowed=" + isBorrowed +
                '}';
    }

    public String getBookName() {
        return BookName;
    }

    public void setBookName(String bookName) {
        BookName = bookName;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public boolean isBorrowed() {
        return isBorrowed;
    }

    public void setBorrowed(boolean borrowed) {
        isBorrowed = borrowed;
    }
}
