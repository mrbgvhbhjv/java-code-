package User;

import ioperation.*;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-09-10
 * Time: 16:15
 */
public class NormalUser extends User{

    public NormalUser(String name) {
        super(name);
        this.iOperations = new IOperation[] {
                new ExitOperation(),
                new FindOperation(),
                new ShowOperation(),
                new BorrowOperation(),
                new ReturnOperation()
        };
    }

    public int menu() {
        System.out.println("欢迎"+this.name+"来到图书管理系统");
        System.out.println("********普通用户菜单：********");
        System.out.println("********1.查找图书：*********");
        System.out.println("********2.显示图书：*********");
        System.out.println("********3.借阅图书：*********");
        System.out.println("********4.归还图书：*********");
        System.out.println("********0.退出系统：*********");
        System.out.println("请输入你的选择：");

        Scanner scanner = new Scanner(System.in);
        int choice = scanner.nextInt();
        return choice;
    }

}
