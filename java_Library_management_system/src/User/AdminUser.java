package User;


import ioperation.*;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-09-10
 * Time: 16:13
 */
public class AdminUser extends User{

    public AdminUser(String name) {
        super(name);
        this.iOperations = new IOperation[] {
            new ExitOperation(),
            new ShowOperation(),
            new FindOperation(),
            new AddOperation(),
            new DelOperation()
        };
    }

    public  int menu() {
        System.out.println("欢迎"+this.name+"来到图书管理系统");
        System.out.println("*********管理员菜单：********");
        System.out.println("********1.显示图书：*********");
        System.out.println("********2.查找图书：*********");
        System.out.println("********3.新增图书：*********");
        System.out.println("********4.删除图书：*********");
        System.out.println("********0.退出系统：*********");
        System.out.println("请输入你的选择：");

        Scanner scanner = new Scanner(System.in);
        int choice = scanner.nextInt();
        return choice;
    }
}
