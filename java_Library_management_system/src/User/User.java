package User;

import book.BookList;
import ioperation.IOperation;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-09-10
 * Time: 16:14
 */
public abstract class User {
    protected String name;
    protected IOperation[] iOperations;

    public User(String name) {
        this.name = name;
    }
    public abstract int menu();
    public void doOperation(int pos, BookList bookList) {
        //调用了数组里面的 匿名对象，通过对象的引用，调用不同操作类的方法，并传参
        iOperations[pos].work(bookList);
    }
}
