package ioperation;

import book.Book;
import book.BookList;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-09-10
 * Time: 19:16
 */
public class DelOperation implements IOperation{
    @Override
    public void work(BookList bookList) {
        System.out.println("删除图书");

        int currentSize = bookList.getUsedSize();

        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入你要删除的图书的书名：");
        String name = scanner.nextLine();
        int pos = -1;
        int i = 0;
        for (; i < currentSize; i++) {
            if(bookList.getBook(i).getBookName().equals(name)) {
                pos = i;
                break;
            }
        }
        if(i == currentSize) {
            System.out.println("没有你要删除的图书。");
            return;
        }
        for (int j = pos; j < currentSize-1; j++) {
            bookList.setBook(j,bookList.getBook(j+1));
        }
        bookList.setBook(currentSize,null);
        bookList.setUsedSize(currentSize -1);
        System.out.println("删除成功！");
    }
}
