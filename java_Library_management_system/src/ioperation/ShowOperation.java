package ioperation;

import book.BookList;
import book.Book;
/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-09-10
 * Time: 19:15
 */
public class ShowOperation implements IOperation{
    @Override
    public void work(BookList bookList) {
        System.out.println("显示图书");
        int currentSize = bookList.getUsedSize();
//        BookList bookList1 = new BookList();
        if(currentSize > 0) {
            for (int i = 0; i < currentSize; i++) {
//            Book book = bookList1.getBook(i);
//            System.out.println(book);
                System.out.println(bookList.getBook(i));
            }
        }else {
            System.out.println("没有图书可以显示！");
            return;
        }

    }
}
