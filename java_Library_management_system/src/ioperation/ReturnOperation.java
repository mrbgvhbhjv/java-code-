package ioperation;

import book.BookList;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-09-10
 * Time: 19:16
 */
public class ReturnOperation implements IOperation{
    @Override
    public void work(BookList bookList) {
        System.out.println("归还图书");

        int currentSize = bookList.getUsedSize();

        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入你要归还的图书的名字：");
        String name = scanner.nextLine();
        for (int i = 0;i < currentSize;i++) {
            if(bookList.getBook(i).getBookName().equals(name)) {
                if(bookList.getBook(i).isBorrowed() == false) {
                    System.out.println("这本书没有被借阅过，你的归还操作无效。");
                    return;
                }
                System.out.println("归还成功！");
                bookList.getBook(i).setBorrowed(false);
                return;
            }
        }
        System.out.println("没有你要归还的书。");
    }
}
