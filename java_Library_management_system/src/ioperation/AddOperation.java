package ioperation;

import book.Book;
import book.BookList;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-09-10
 * Time: 19:15
 */
public class AddOperation implements IOperation {
    @Override
    public void work(BookList bookList) {
        System.out.println("新增图书");

        int currentSize = bookList.getUsedSize();
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入你要新增的图书的书名：");
        String name = scanner.nextLine();
        System.out.println("请输入你要新增的图书的作者：");
        String author = scanner.nextLine();
        System.out.println("请输入你要新增的图书的类型：");
        String type = scanner.nextLine();
        System.out.println("请输入你要新增的图书的价格：");
        int price = scanner.nextInt();
        for (int i = 0; i < currentSize; i++) {
            if(bookList.getBook(i).getBookName().equals(name)) {
                System.out.println("这本图书已经存在，无法新增！");
                return;
            }
        }
        Book book =new Book(name,author,type,price);
        bookList.setBook(currentSize,book);
        book.setBorrowed(false);
        bookList.setUsedSize(currentSize + 1);

        System.out.println("成功添加图书。");
    }
}
