package ioperation;

import book.BookList;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-09-10
 * Time: 19:17
 */
public interface IOperation {
    void work(BookList bookList);
}
