package ioperation;

import book.BookList;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-09-10
 * Time: 19:16
 */
public class BorrowOperation implements IOperation{
    @Override
    public void work(BookList bookList) {
        System.out.println("借阅图书");

        int currentSize = bookList.getUsedSize();

        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入你要借阅的图书的名字：");
        String name = scanner.nextLine();
        for (int i = 0;i < currentSize;i++) {
            if(bookList.getBook(i).getBookName().equals(name)) {
                if(bookList.getBook(i).isBorrowed() == true) {
                    System.out.println("你要借阅的书，已经被借阅了，无法借阅！");
                    return;
                }
                System.out.println("找到了你要借阅的图书，借阅成功！");
                bookList.getBook(i).setBorrowed(true);
                return;
            }
        }
        System.out.println("没有你要借阅的书。");
    }
}
