package ioperation;

import book.BookList;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 何华树
 * Date: 2024-09-10
 * Time: 19:14
 */
public class FindOperation implements IOperation{
    @Override
    public void work(BookList bookList) {
        System.out.println("查找图书");

        int currentSize = bookList.getUsedSize();

        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入你要查找的图书的书名：");
        String name = scanner.nextLine();

        for (int i = 0; i < currentSize; i++) {
            if(bookList.getBook(i).getBookName().equals(name)) {
                System.out.println("找到这本书了。");
                System.out.println(bookList.getBook(i));
                return;
            }
        }
        System.out.println("没有你要找的书。");
    }
}
